
# A very simple Flask Hello World app for you to get started with...

from flask import Flask, request, render_template, jsonify, render_template
from flask_mysqldb import MySQL
from pivottablejs import pivot_ui
import io
import base64
import time
import matplotlib.pyplot as plt
from flask import Flask, render_template, request, Response
from datetime import datetime
import plotly.graph_objs as go
import plotly.offline as pyo
import plotly.express as px

app = Flask(__name__)

# MySQL configurations
app.config['MYSQL_HOST'] = 'qias.mysql.pythonanywhere-services.com'
app.config['MYSQL_USER'] = 'qias'
app.config['MYSQL_PASSWORD'] = 'qias2020'
app.config['MYSQL_DB'] = 'qias$test2'

mysql = MySQL(app)
@app.before_request
def handle_chunking():
#    """
#    Sets the "wsgi.input_terminated" environment flag, thus enabling
#    Werkzeug to pass chunked requests as streams.  The gunicorn server
#    should set this, but it's not yet been implemented.
#    """

    transfer_encoding = request.headers.get("Transfer-Encoding", None)
    if transfer_encoding == u"chunked":
        request.environ["wsgi.input_terminated"] = False
def type_mapper(val):
    """Map Python types to MySQL types."""
    if isinstance(val, int):
        return 'INT'
    elif isinstance(val, float):
        return 'FLOAT'
    elif isinstance(val, str):
        return 'VARCHAR(255)'
    elif isinstance(val, datetime):
        return 'DATETIME'
    elif isinstance(val, bool):
        return 'TINYINT(1)'
    else:
        return 'TEXT'
def check_and_create_table(table_name, data_element):
    """Check if table exists and create if not."""

    cur = mysql.connection.cursor()

    # Check if table exists
    cur.execute(f"SHOW TABLES LIKE '{table_name}'")
    if cur.fetchone():
        print(f"Table {table_name} already exists.")
        return

    # Infer schema from the first dictionary
    first_item = data_element
    columns = ', '.join([f"{key} {type_mapper(value)}" for key, value in first_item.items()])

    # Create table
    create_table_query = f"CREATE TABLE {table_name} ({columns})"
    cur.execute(create_table_query)
    mysql.connection.commit()
    print(f"Table {table_name} created successfully.")

    cur.close()
def check_and_create_table(table_name, data_element):
    """Check if table exists and create if not."""

    cur = mysql.connection.cursor()

    # Check if table exists
    cur.execute(f"SHOW TABLES LIKE '{table_name}'")
    if cur.fetchone():
        print(f"Table {table_name} already exists.")
        return

    # Infer schema from the first dictionary
    first_item = data_element
    columns = []

    for key, value in first_item.items():
        if key == 'datetime':
            columns.append(f"{key} DATE")
        elif key == 'time':
            columns.append(f"{key} TIME")
        elif key == 'date':
            columns.append(f"{key} DATE")
        else:
            columns.append(f"{key} {type_mapper(value)}")

    # Create table
    create_table_query = f"CREATE TABLE {table_name} ({', '.join(columns)})"
    cur.execute(create_table_query)
    mysql.connection.commit()
    print(f"Table {table_name} created successfully.")

    cur.close()
def check_and_create_table(table_name, data_element):
    """Check if table exists and create if not."""

    cur = mysql.connection.cursor()

    # Check if table exists
    cur.execute(f"SHOW TABLES LIKE '{table_name}'")
    if cur.fetchone():
        print(f"Table {table_name} already exists.")
        return

    # Infer schema from the first dictionary
    first_item = data_element
    columns = []

    for key, value in first_item.items():
        if key == 'datetime':
            columns.append(f"{key} DATE")
        elif key == 'time':
            columns.append(f"{key} TIME")
        elif key == 'date':
            columns.append(f"{key} DATE")
        else:
            columns.append(f"{key} {type_mapper(value)}")

    # Create table
    create_table_query = f"CREATE TABLE {table_name} ({', '.join(columns)})"
    cur.execute(create_table_query)
    mysql.connection.commit()
    print(f"Table {table_name} created successfully.")

    cur.close()
@app.route('/store', methods=['POST'])
def store():
    if request.method == 'POST':
        data = request.form
        name = data['name']
        email = data['email']

        cur = mysql.connection.cursor()
        cur.execute("INSERT INTO users(name, email) VALUES(%s, %s)", (name, email))
        mysql.connection.commit()
        cur.close()

        return 'Data stored successfully!'

import pandas as pd
@app.route('/postlist', methods=['POST'])
def post_list():
    data = request.json
    received_list = data['list']
    check_and_create_table("qias_table",received_list[0])
    cur = mysql.connection.cursor()
    for i in received_list:
     keys = ",".join(i.keys())
     placeholders = ",".join(["%s"] * len(i))
     query = f"INSERT INTO qias_table({keys}) VALUES({placeholders})"
     cur.execute(query, list(i.values()))
    mysql.connection.commit()
    received_list = data['list2']
    check_and_create_table("qias_table2",received_list[0])
    cur = mysql.connection.cursor()
    for i in received_list:
     keys = ",".join(i.keys())
     placeholders = ",".join(["%s"] * len(i))
     query = f"INSERT INTO qias_table2({keys}) VALUES({placeholders})"
     cur.execute(query, list(i.values()))
    mysql.connection.commit()
    return jsonify({"message": "List received successfully!"})

@app.route('/show')
def show():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM qias_table")
    rows = cur.fetchall()
    column_names = [desc[0] for desc in cur.description]
    cur.close()

    # Convert rows to list of dictionaries
    data = [dict(zip(column_names, row)) for row in rows]

    # Convert data to pandas DataFrame and then to HTML
    df = pd.DataFrame(data).iloc[::-1]
    table_html = df.to_html(classes='dataframe', border=0)

    return  table_html
@app.route('/show2')
def show2():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM qias_table2")
    rows = cur.fetchall()
    column_names = [desc[0] for desc in cur.description]
    cur.close()

    # Convert rows to list of dictionaries
    data = [dict(zip(column_names, row)) for row in rows]

    # Convert data to pandas DataFrame and then to HTML
    df = pd.DataFrame(data).iloc[::-1]
    table_html = df.to_html(classes='dataframe', border=0)

    return  table_html


@app.route('/errors')
def list_errors():
    return "<br>".join(error_list)

@app.route('/test')
def test():
 return ""

# Create a list to store errors
error_list = []
@app.route("/dash")
def dash():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM qias_table")
    rows = cur.fetchall()
    column_names = [desc[0] for desc in cur.description]
    cur.close()

    # Convert rows to list of dictionaries
    data = [dict(zip(column_names, row)) for row in rows]

    # Convert data to pandas DataFrame and then to HTML
    return pivot_ui(pd.DataFrame(data).iloc[::-1])
@app.route("/dash2")
def dash2():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM qias_table2")
    rows = cur.fetchall()
    column_names = [desc[0] for desc in cur.description]
    cur.close()

    # Convert rows to list of dictionaries
    data = [dict(zip(column_names, row)) for row in rows]

    # Convert data to pandas DataFrame and then to HTML
    return pivot_ui(pd.DataFrame(data).iloc[::-1])

@app.route("/")
def index():
    cursor=mysql.connection.cursor()
    # Fetch the minimum and maximum datetime values from the table
    cursor.execute("SELECT MIN(datetime), MAX(datetime) FROM qias_table")
    date_range = cursor.fetchone()

    # Convert the datetime values to string format for the input fields
    min_date = date_range[0]
    max_date = date_range[1]
    cursor.execute("DESCRIBE qias_table")
    columns = [row[0] for row in cursor.fetchall()]
    cursor.close()
    return render_template("index.html", min_date=min_date, max_date=max_date, columns=columns)

def transform_datetime(input_str):
    return datetime.datetime.strptime(input_str)
    try:
        # Parse the input string into a datetime object
        input_datetime = datetime.strptime(input_str, '%Y-%m-%dT%H:%M:%S.%f')

        # Format the datetime object into the desired output format
        output_str = input_datetime.strftime('%Y-%m-%d %H:%M:%S')

        return output_str
    except ValueError:
        # Handle the case where the input string is not in the expected format
        return "Invalid input format. Please provide a valid 'YYYY-MM-DDTHH:MM:SS.ssssss' format."

def chart(svg):

 return'''<!DOCTYPE html>
<html>
<head>
    <title>SVG Embed Example</title>
</head>
<body>
    <!-- Using an <img> element to embed the SVG -->
    <img src="/get_svg" alt="Embedded SVG">

    <!-- Using an <object> element to embed the SVG -->
    <object data="'''+svg+'''" type="image/svg+xml" width="400" height="300">
        Your browser does not support SVG
    </object>
</body>
</html>'''
@app.route("/generate_chartold", methods=["GET"])
def generate_chartold():
    cursor=mysql.connection.cursor()

    start_date = request.args.get("start_date","2023-09-22 12:1:1")
    end_date = request.args.get("end_date","2023-09-24 12:1:1")
    print((start_date, end_date))
    # Fetch data from the MySQL database based on the selected date-time range
    query = """
     SELECT SUM(nombre_homme) AS NH, SUM(nombre_femme) AS NF, SUM(nombre_attention) AS NA
    FROM qias_table
    WHERE datetime BETWEEN '"""+start_date+"""' AND '"""+end_date+"""'   """
    print("+++++++",query,"---------------")
    cursor.execute(query)
    result = cursor.fetchall()

    print(str(result[0]),type(result))

    if result:
        NH = result[0][0]
        NF = result[0][1]
        NA = result[0][2]
        NH_percentage = (NH / NA) * 100
        NF_percentage = (NF / NA) * 100

        # Create a pie chart
        labels = ['NH', 'NF']
        sizes = [NH_percentage, NF_percentage]
        colors = ['blue', 'pink']
        pie_data = {
        'labels': ['Taux convertion hommes', 'Taux convertion femmes','reste'],
        'values': [NH_percentage,NF_percentage,(100-(NH_percentage+NF_percentage))],
    }
        # Return the SVG data as a response
        return render_template('chart.html', pie_data=pie_data)
categories = ['Category 1', 'Category 2', 'Category 3', 'Category 4', 'Category 5']
bar_data = [10, 25, 15, 30, 20]
line_data = [5, 15, 10, 20, 12]

@app.route('/linechart')
def linechart():
    # Create bar chart using Plotly Express
    bar_fig = px.bar(x=categories, y=bar_data, title='Bar Chart')

    # Create line chart using Plotly Graph Objects
    line_trace = go.Scatter(x=categories, y=line_data, mode='lines+markers', name='Line Chart')
    line_layout = go.Layout(title='Line Chart', xaxis=dict(title='Categories'), yaxis=dict(title='Values'))
    line_fig = go.Figure(data=[line_trace], layout=line_layout)

    return render_template('linechart1.html', bar_chart=bar_fig.to_html(), line_chart=line_fig.to_html())

@app.route("/generate_chart", methods=["GET"])
def generate_chart():
    cursor = mysql.connection.cursor()

    start_date = request.args.get("start_date", "2023-09-22 12:1:1")
    end_date = request.args.get("end_date", "2023-09-24 12:1:1")
    columns = request.args.get("columns", "'['nombre_femme', 'nombre_homme', 'nombre_attention']'")
    print(columns[1:-1],"+++++++++++")
    # Convert the 'columns' parameter from a string to a list
    columns = eval(columns[1:-1])

    # Construct the dynamic SELECT statement and columns for values
    select_columns = ', '.join([f"SUM({column}) AS {column}" for column in columns])
    values_columns = ', '.join(columns)

    # Fetch data from the MySQL database based on the selected date-time range and columns
    query = f"""
     SELECT {select_columns}
    FROM qias_table
    WHERE datetime BETWEEN '{start_date}' AND '{end_date}'
    """

    cursor.execute(query)
    result = cursor.fetchall()

    if result:
        # Extract values for the specified columns
        values = [result[0][i] for i in range(len(columns))]

        # Calculate percentages for each column
        total = sum(values)
        percentages = [(value / total) * 100 for value in values]

        # Create a pie chart
        labels = columns
        sizes = percentages
        colors = ['blue', 'pink', 'gray']  # You can customize colors as needed

        pie_data = {
            'labels': labels,
            'values': sizes,
        }

        # Return the SVG data as a response
        return render_template('chart.html', pie_data=pie_data)
@app.route('/generate_linebar')
def linebar():
    # Create sample data for the charts
    cursor = mysql.connection.cursor()

    start_date = request.args.get("start_date", "2023-09-22 12:1:1")
    end_date = request.args.get("end_date", "2023-09-24 12:1:1")
    columns = request.args.get("columns", "'['nombre_femme', 'nombre_homme', 'nombre_temps_attention']'")
    print(columns[1:-1],"+++++++++++")
    # Convert the 'columns' parameter from a string to a list
    columns = eval(columns[1:-1])
       # Fetch all the rows that match the columns

    query = f"SELECT {columns[-1]} FROM qias_table"
    cursor.execute(query)        # Fetch all the rows that match the columns
    y_line_chart = [item[0] for item in cursor.fetchall()]
    y_bar_chart=[]
    for j in columns[:-1]:
     query = f"SELECT {j} FROM qias_table"
     cursor.execute(query)
     cf=cursor.fetchall()
     cf=[item[0] for item in cf]
     print(type(cf),cf)
     y_bar_chart.append(sum(cf))# Fetch all the rows that match the columns

    x_data = columns
    # Create a line chart
    trace_line = go.Scatter(x=x_data, y=y_line_chart, mode='lines+markers', name='Line Chart')

    # Create a bar chart
    trace_bar = go.Bar(x=x_data, y=y_bar_chart, name='Bar Chart')

    # Create a subplot with both charts
    fig = go.Figure(data=[trace_line, trace_bar])

    # Convert the Plotly figure to JSON
    plot_json = pyo.plot(fig, output_type='div', include_plotlyjs=False)

    return render_template('linebar.html', plot_json=plot_json)
@app.route('/add_error', methods=['POST'])
def add_error():
    error_message = request.form.get('error_message')
    if error_message:
        error_list.append(error_message)
        return 'Error added to the list.'
    else:
        return 'Error message missing.', 400
