import 'dart:ffi' ;
import 'dart:io';
import 'dart:ui' as me;
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_mlkit_commons/google_mlkit_commons.dart';
import 'package:tflite_flutter/tflite_flutter.dart';
import 'package:tflite_flutter_helper/tflite_flutter_helper.dart';
import 'dart:typed_data';  // You'll need this for Float32List
import 'package:image/image.dart' as img;
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';
import 'database_helper.dart';
import 'dart:math';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:device_info_plus/device_info_plus.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'package:path_provider/path_provider.dart';

import 'package:flutter/services.dart'; // For platform-specific code
import 'package:flutter/services.dart';
import 'dart:developer' as developer;
import 'package:logging/logging.dart';
import 'dart:io';
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Logger.root.level = Level.ALL; // Set the desired log level
  Logger.root.onRecord.listen((record) {
    // Print logs to the console
    print('[${record.level.name}] ${record.time}: ${record.message}');

    // Write logs to a file
    sendErrorToFlaskerr(record.message);
  });
  FlutterError.onError = (details) {
    sendErrorToFlask(details.stack.toString());
    FlutterError.presentError(details);
    if (kReleaseMode) exit(1);
  };
  runApp(CameraView());
}
var interpreter;
var croppedFace;
var interpreter2;







Future<void> sendErrorToFlaskold(String errorMessage) async {
  try {
    final url = Uri.parse('https://qias.pythonanywhere.com/add_error');
    final response = await http.post(url, body: {'error_message': errorMessage.toString()});
    if (response.statusCode == 200) {
      // Error sent successfully
    } else {
      // Handle error cases when sending the error
      print('Failed to send error: ${response.statusCode}');
    }
  } catch (e) {
    // Handle network errors or exceptions when sending the error
    print('Error sending error : $e');
  }
 // print("eroring"+errorMessage);
  stderr.writeln("eroring"+errorMessage);

}


Future<void> sendErrorToFlask(String textToAppend) async {

  try {
    Directory documentsDirectory;

    if (Platform.isAndroid) {
      documentsDirectory = Directory('/storage/emulated/0/Documents');
    } else if (Platform.isIOS ) {
      documentsDirectory = await getApplicationDocumentsDirectory();
    } else {
      throw Exception("Unsupported platform");
    }

    final filePath = '${documentsDirectory.path}/log.txt';
    final file = File(filePath);

    if (!file.existsSync()) {
      file.createSync();
    }

    file.writeAsStringSync('$textToAppend\n', mode: FileMode.append);
    stderr.writeln("eroring : "+textToAppend);
    print("eroring : "+textToAppend);

    // print('Text appended to the end of the "log.txt" file in the "Documents" directory.');
  } catch (e) {
    print('Error: $e');
  }
}
Future<void> sendErrorToFlaskerr(String textToAppend) async {

  try {
    Directory documentsDirectory;

    if (Platform.isAndroid) {
      documentsDirectory = Directory('/storage/emulated/0/Documents');
    } else if (Platform.isIOS ) {
      documentsDirectory = await getApplicationDocumentsDirectory();
    } else {
      throw Exception("Unsupported platform");
    }

    final filePath = '${documentsDirectory.path}/err.txt';
    final file = File(filePath);

    if (!file.existsSync()) {
      file.createSync();
    }

    file.writeAsStringSync('$textToAppend\n', mode: FileMode.append);
    stderr.writeln("eroring : "+textToAppend);

    // print('Text appended to the end of the "log.txt" file in the "Documents" directory.');
  } catch (e) {
    print('Error: $e');
  }
}








  class ExtendedInputImage {
  final InputImage inputImage;
  final String extraStringData;
  final List<String> extraArrayData;

  ExtendedInputImage({
    required this.inputImage,
    required this.extraStringData,
    required this.extraArrayData,
  });
}
class CameraView extends StatefulWidget {
  CameraView(
      {Key? key})
      : super(key: key);

  /// final Function(InputImage inputImage) onImage;
//  final VoidCallback? onCameraFeedReady;
  // final VoidCallback? onDetectorViewModeChanged;
  // final Function(CameraLensDirection direction)? onCameraLensDirectionChanged;
//  final CameraLensDirection initialCameraLensDirection;

  @override
  State<CameraView> createState() => _CameraViewState();
}

class _CameraViewState extends State<CameraView> {
  static List<CameraDescription> _cameras = [];
  static const platform = const MethodChannel('google_mlkit_face_detector_agegender');
  var dn=DateTime.now();
  var inserts=0;
  var vars={
    'nombre_senior':0,
    'nombre_Adult':0,
    'nombre_jeune_Adulte':0,
    'nombre_jeune':0,
    'nombre_moyen_age':0,



    'moyen_senior':0,
    'moyen_Adult':0,
    'moyen_jeune_Adulte':0,
    'moyen_jeune':0,
    'moyen_moyen_age':0,


    'nombre_homme':0,
    'nombre_femme':0,

    'moyen_homme':0,
    'moyen_femme':0,




    'nombre_heureux':0,
    'nombre_content':0,
    'nombre_neutre':0,
    'nombre_mecontent':0,

    'moyen_heureux':0,
    'moyen_content':0,
    'moyen_neutre':0,
    'moyen_mecontent':0,



    'min_concentration':0,
    'moyen_concentration':0,

    'nombre_temps_attention':0,
    'nombre_temps_inattention':0,


    'nombre_ots':0,
    'nombre_attention':0,
    'nombre_inattention':0,
    'moyen_attention':0,
    'moyen_inattention':0,
  };
  var varsc={
    'cumul_attention':0,
    'cumul_inattention':0,
    'cumul_temps_attention':0,
    'cumul_temps_inattention':0,
    'cumul_ots':0,
    'cumul_temps_ots':0,

    'cumul_senior':0,
    'cumul_Adult':0,
    'cumul_jeune_Adulte':0,
    'cumul_jeune':0,
    'cumul_moyen_age':0,

    'cumul_heureux':0,
    'cumul_content':0,
    'cumul_neutre':0,
    'cumul_mecontent':0,

    'cumul_homme':0,
    'cumul_femme':0,
  };
  var varsd;
  final dbHelper = DatabaseHelper.instance;
  void requestCameraPermission() async {
    var status = await Permission.camera.status;
    if (status.isDenied) {
      // You can show a dialog or explanation here to inform the user about why you need the camera permission.
      // Then, request the permission.
      var result = await Permission.camera.request();

      if (result.isGranted) {
        // Permission is granted; you can now access the camera.
      } else {
        // Permission is denied; handle it accordingly.
      }
    } else if (status.isGranted) {
      // Permission is already granted; you can access the camera.
    } else if (status.isPermanentlyDenied) {
      // The user has permanently denied camera permission. You may need to guide the user to the app settings to manually enable it.
    }
    status = await Permission.storage.status;
    if (status.isDenied) {
      // You can show a dialog or explanation here to inform the user about why you need the camera permission.
      // Then, request the permission.
      var result = await Permission.storage.request();

      if (result.isGranted) {
        // Permission is granted; you can now access the camera.
      } else {
        // Permission is denied; handle it accordingly.
      }
    } else if (status.isGranted) {
      // Permission is already granted; you can access the camera.
    } else if (status.isPermanentlyDenied) {
      // The user has permanently denied camera permission. You may need to guide the user to the app settings to manually enable it.
    }
  }
  img.Image convertBGRA8888ToImage(CameraImage image) {
    final int width = image.width;
    final int height = image.height;

    // Create a blank image with the dimensions
    var imgLib = img.Image(width, height);

    final Uint8List data = image.planes[0].bytes;
    final int length = width * height;

    for (int i = 0; i < length; i++) {
      final int offset = i * 4;

      // Extract the BGRA values
      final int blue = data[offset];
      final int green = data[offset + 1];
      final int red = data[offset + 2];
      final int alpha = data[offset + 3];

      // Set the pixel values
      imgLib.setPixelRgba(i % width, i ~/ width, red, green, blue, alpha);
    }

    return imgLib;
  }
  _insert() async {
    sendErrorToFlask("starting insertion");
    varsd=varsc;
    vars['nombre_temps_attention']=DateTime.now().difference(dn).inMilliseconds.round()*vars['nombre_attention']!;
    vars['nombre_temps_inattention']=DateTime.now().difference(dn).inMilliseconds.round()*vars['nombre_inattention']!;

    var devicename= await fetchDeviceId();
    if(vars['nombre_ots']!>0){
      vars['moyen_attention']=(((vars['nombre_attention']!/vars['nombre_ots']!))*100).round();
      vars['moyen_inattention']=(((vars['nombre_inattention']!/vars['nombre_ots']!))*100).round();
      vars['moyen_senior']=(((vars['nombre_senior']!/vars['nombre_ots']!))*100).round();
      vars['moyen_Adult']=(((vars['nombre_Adult']!/vars['nombre_ots']!))*100).round();
      vars['moyen_jeune_Adulte']=(((vars['nombre_jeune_Adulte']!/vars['nombre_ots']!))*100).round();
      vars['moyen_jeune']=(((vars['nombre_jeune']!/vars['nombre_ots']!))*100).round();
      vars['moyen_homme']=(((vars['nombre_homme']!/vars['nombre_ots']!))*100).round();
      vars['moyen_femme']=(((vars['nombre_femme']!/vars['nombre_ots']!))*100).round();

      vars['moyen_heureux']=(((vars['nombre_heureux']!/vars['nombre_ots']!))*100).round();
      vars['moyen_content']=(((vars['nombre_content']!/vars['nombre_ots']!))*100).round();
      vars['moyen_neutre']=(((vars['nombre_neutre']!/vars['nombre_ots']!))*100).round();
      vars['moyen_mecontent']=(((vars['nombre_mecontent']!/vars['nombre_ots']!))*100).round();

      varsd['cumul_attention']=vars['nombre_attention']!+varsd['cumul_attention']!;
      varsd['cumul_inattention']=vars['nombre_inattention']!+varsd['cumul_inattention']!;
      varsd['cumul_senior']=vars['nombre_senior']!+varsd['cumul_senior']!;
      varsd['cumul_Adult']=vars['nombre_Adult']!+varsd['cumul_Adult']!;
      varsd['cumul_jeune_Adulte']=vars['nombre_jeune_Adulte']!+varsd['cumul_jeune_Adulte']!;
      varsd['cumul_jeune']=vars['nombre_jeune']!+varsd['cumul_jeune']!;
      varsd['cumul_homme']=vars['nombre_homme']!+varsd['cumul_homme']!;
      varsd['cumul_femme']=vars['nombre_femme']!+varsd['cumul_femme']!;
      varsd['cumul_heureux']=vars['nombre_heureux']!+varsd['cumul_heureux']!;
      varsd['cumul_content']=vars['nombre_content']!+varsd['cumul_content']!;
      varsd['cumul_neutre']=vars['nombre_neutre']!+varsd['cumul_neutre']!;
      varsd['cumul_mecontent']=vars['nombre_mecontent']!+varsd['cumul_mecontent']!;
      varsd['cumul_neutre']=vars['nombre_neutre']!+varsd['cumul_neutre']!;
      varsd['cumul_moyen_age']=vars['moyen_moyen_age']!+varsd['cumul_moyen_age']!;

      varsd['cumul_temps_attention']=vars['nombre_temps_attention']!+varsd['cumul_temps_attention']!;
      varsd['cumul_temps_inattention']=vars['nombre_temps_inattention']!+varsd['cumul_temps_inattention']!;
      varsd['cumul_moyen_age']=((vars['moyen_moyen_age']!+varsd['cumul_moyen_age']!)/2).ceil();
    };


    Map<String, dynamic> row = {


      DatabaseHelper.column_nombre_senior : vars['nombre_senior'],
      DatabaseHelper.column_nombre_Adult : vars['nombre_Adult'],
      DatabaseHelper.column_nombre_jeune_Adulte : vars['nombre_jeune_Adulte'],
      DatabaseHelper.column_nombre_jeune : vars['nombre_jeune'],
      DatabaseHelper.column_nombre_moyen_age : vars['nombre_moyen_age'],



      DatabaseHelper.column_moyen_senior : vars['moyen_senior'],
      DatabaseHelper.column_moyen_Adult : vars['moyen_Adult'],
      DatabaseHelper.column_moyen_jeune_Adulte : vars['moyen_jeune_Adulte'],
      DatabaseHelper.column_moyen_jeune : vars['moyen_jeune'],
      DatabaseHelper.column_moyen_moyen_age : vars['moyen_moyen_age'],


      DatabaseHelper.column_nombre_homme : vars['nombre_homme'],
      DatabaseHelper.column_nombre_femme : vars['nombre_femme'],

      DatabaseHelper.column_moyen_homme : vars['moyen_homme'],
      DatabaseHelper.column_moyen_femme : vars['moyen_femme'],




      DatabaseHelper.column_nombre_heureux : vars['nombre_heureux'],
      DatabaseHelper.column_nombre_content : vars['nombre_content'],
      DatabaseHelper.column_nombre_neutre : vars['nombre_neutre'],
      DatabaseHelper.column_nombre_mecontent : vars['nombre_mecontent'],

      DatabaseHelper.column_moyen_heureux : vars['moyen_heureux'],
      DatabaseHelper.column_moyen_content : vars['moyen_content'],
      DatabaseHelper.column_moyen_neutre : vars['moyen_neutre'],
      DatabaseHelper.column_moyen_mecontent : vars['moyen_mecontent'],



      DatabaseHelper.column_min_concentration : vars['min_concentration'],
      DatabaseHelper.column_moyen_concentration : vars['moyen_concentration'],

      DatabaseHelper.column_nombre_temps_attention : vars['nombre_temps_attention'],
      DatabaseHelper.column_nombre_temps_inattention : vars['nombre_temps_inattention'],


      DatabaseHelper.column_nombre_ots : vars['nombre_ots'],
      DatabaseHelper.column_nombre_attention : vars['nombre_attention'],
      DatabaseHelper.column_nombre_inattention : vars['nombre_inattention'],
      DatabaseHelper.column_moyen_attention : vars['moyen_attention'],
      DatabaseHelper.column_moyen_inattention : vars['moyen_inattention'],



      DatabaseHelper.column_cumul_attention : varsd['cumul_attention'],
      DatabaseHelper.column_cumul_inattention : varsd['cumul_inattention'],
      DatabaseHelper.column_cumul_temps_attention : varsd['cumul_temps_attention'],
      DatabaseHelper.column_cumul_temps_inattention : varsd['cumul_temps_inattention'],
      DatabaseHelper.column_cumul_ots : varsd['cumul_ots'],
      DatabaseHelper.column_cumul_temps_ots : varsd['cumul_temps_ots'],

      DatabaseHelper.column_cumul_senior : varsd['cumul_senior'],
      DatabaseHelper.column_cumul_Adult : varsd['cumul_Adult'],
      DatabaseHelper.column_cumul_jeune_Adulte : varsd['cumul_jeune_Adulte'],
      DatabaseHelper.column_cumul_jeune : varsd['cumul_jeune'],
      DatabaseHelper.column_cumul_moyen_age : varsd['cumul_moyen_age'],

      DatabaseHelper.column_cumul_heureux : varsd['cumul_heureux'],
      DatabaseHelper.column_cumul_content : varsd['cumul_content'],
      DatabaseHelper.column_cumul_neutre : varsd['cumul_neutre'],
      DatabaseHelper.column_cumul_mecontent : varsd['cumul_mecontent'],

      DatabaseHelper.column_cumul_homme : varsd['cumul_homme'],
      DatabaseHelper.column_cumul_femme : varsd['cumul_femme'],



      DatabaseHelper.columnNomAppareil:devicename,
      DatabaseHelper.columnDate:dn.year.round().toString()+"-"+dn.month.round().toString()+"-"+dn.day.round().toString(),
      DatabaseHelper.columntime:dn.hour.round().toString()+":"+dn.minute.round().toString()+":"+dn.second.round().toString(),
      DatabaseHelper.columnDatetime:dn.toIso8601String()
    };
    // Change the text dynamically here.
        ;

//print(row);
    if(vars['nombre_ots']!>-1){
      final id = await dbHelper.insert(row);
      sendErrorToFlask("starting inserted");

    }else{
      sendErrorToFlask("no detection");
    };
    //print('inserted row id: $inserts');
    inserts++;
    if (inserts%400==0){inserts=0;print("sending");await _query();    sendErrorToFlask("sending");
    }
    else if (inserts%200==0){print("commiting"); _commit();}    sendErrorToFlask("commiting");
    ;
  }
  _commit() async {

    final id = await dbHelper.commit();
    sendErrorToFlask("committing");

    // print('inserted row id: $id');
  }
  _query() async {
    List<Map<String, dynamic>> allRows = await dbHelper.queryAllRowsd();
    print(allRows.last);
    postListToServer(allRows);
    sendErrorToFlask("data sended succesfully");

    //  print('query all rows:');
    // print(allRows.length);
    //   print('querried');

    // allRows.forEach((row) => print(row));
  }
  CameraController? _controller;
  var interpreter;
  var interpreter2;
  var _cameraLensDirection = CameraLensDirection.front;
  var formattedImage;
  var inputImage;
  int _cameraIndex = -1;
  double _currentZoomLevel = 1.0;
  double _minAvailableZoom = 1.0;
  double _maxAvailableZoom = 1.0;
  double _minAvailableExposureOffset = 0.0;
  double _maxAvailableExposureOffset = 0.0;
  double _currentExposureOffset = 0.0;
  bool _changingCameraLens = false;
  String dynamicText = 'Google ML Kit Demo App';


  final _faceDetector = FaceDetector(
    options: FaceDetectorOptions(
      enableContours: true,
      enableLandmarks: true,
      enableClassification: true,
      minFaceSize: 0.3,

    ),
  );
  Future<String?> fetchDeviceId() async {
    final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    try {
      AndroidDeviceInfo androidInfo = await deviceInfoPlugin.androidInfo;
      return androidInfo.id;  // Returns the Android ID
    } catch (androidError) {
      try {
        IosDeviceInfo iosInfo = await deviceInfoPlugin.iosInfo;
        return iosInfo.identifierForVendor;  // Returns the Identifier for Vendor (iOS)
      } catch (iosError) {
        print("Failed to get device ID: $iosError");
      }
      print("Failed to get device ID: $androidError");
    }
    return "inconnu";
  }
  Future<void> postListToServer(List<dynamic> myList) async {
    final url = 'https://qias.pythonanywhere.com/postlist';
    //  print(myList);
    final response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'application/json',
      },
      body: jsonEncode({
        'list': myList,
      }),
    );

    if (response.statusCode == 200) {
      print('List posted successfully!');
    } else {
      print('Failed to post the list.');
    }
  }
  @override
  void initState()  {
    super.initState();


    _initialize();
    // List<Map<String, dynamic>> allRows = await dbHelper.queryAllRows();
    //  varsd=allRows.last;
  }

  void _initialize() async {


    try{
      requestCameraPermission();    sendErrorToFlask("permition granted");
    }catch(e){print(e);};

    if (_cameras.isEmpty) {
      sendErrorToFlask("selecting camera");

      WidgetsFlutterBinding.ensureInitialized();

      _cameras = await availableCameras();
    }
    var i=1;

    _cameraIndex = i;
    sendErrorToFlask("camera front selected");


    if (_cameraIndex != -1) {
      _startLiveFeed();
      sendErrorToFlask("detection started");

    }
  }

  @override
  void dispose() {
    _stopLiveFeed();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Flutter App",
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text("Home Page"),
        ),
        body: new Center(child:  _liveFeedBody(),),
      ),
    );
  }

  Widget _liveFeedBody() {
    if (_cameras.isEmpty) return  Text('camera is empty');

    if (_controller == null) return Text('no controller');

    if (_controller?.value.isInitialized == false) return Text('camera created but not initialised');
    return Text('Camera ready!');}



  Future _startLiveFeed() async {
  //  final DynamicLibrary tfliteLib = Platform.isAndroid
    //    ? DynamicLibrary.open("libtensorflowlite_c.so")
      //  : DynamicLibrary.process();
    sendErrorToFlask("models importing");

      if (Platform.isIOS ) {
        interpreter =  await Interpreter.fromAsset('ml/model_age_q.tflite');
        interpreter2 = await Interpreter.fromAsset('ml/model_gender_q.tflite');      } else if (Platform.isAndroid) {


      };

    sendErrorToFlask("models loaded");
    sendErrorToFlask("model imported");

    final camera = _cameras[_cameraIndex];
    _controller = CameraController(
      camera,
      // Set to ResolutionPreset.high. Do NOT set it to ResolutionPreset.max because for some phones does NOT work.
      ResolutionPreset.high,
      enableAudio: false,
      imageFormatGroup: Platform.isAndroid
          ? ImageFormatGroup.nv21
          : ImageFormatGroup.bgra8888,
    );
    sendErrorToFlask("camera initialized");

    _controller?.initialize().then((_) {
      if (!mounted) {
        return;
      }
      _controller?.getMinZoomLevel().then((value) {
        _currentZoomLevel = value;
        _minAvailableZoom = value;
      });
      _controller?.getMaxZoomLevel().then((value) {
        _maxAvailableZoom = value;
      });
      _currentExposureOffset = 0.0;
      _controller?.getMinExposureOffset().then((value) {
        _minAvailableExposureOffset = value;
      });
      _controller?.getMaxExposureOffset().then((value) {
        _maxAvailableExposureOffset = value;
      });
      _controller?.startImageStream(_processCameraImage);
      setState(() {});
    });    sendErrorToFlask("camera configured");

  }

  Future _stopLiveFeed() async {
    await _controller?.stopImageStream();
    await _controller?.dispose();
    _controller = null;
  }

  Future _switchLiveCamera() async {
    setState(() => _changingCameraLens = true);
    _cameraIndex = (_cameraIndex + 1) % _cameras.length;

    await _stopLiveFeed();
    await _startLiveFeed();
    setState(() => _changingCameraLens = false);
  }

  void _processCameraImage(CameraImage image) {
    //  print(image.height);
    // print(image.width);

    sendErrorToFlask("starting image processing");

    if (Platform.isAndroid) {
      formattedImage = convertYUV420ToImage(image);
    } else {
      formattedImage = convertBGRA8888ToImage(image);
    }


    //  input,
    //    ageOutputArray
    //  );

    // var processedImageBuffer =imageProcessor.process(tensorData).buffer;
    // var ageOutputArray = List.generate(1, (i) => List.filled(1, 0.0));
    // interpreter?.run(
    //   processedImageBuffer,
    //  ageOutputArray
    // );
    //print(ageOutputArray[0][0] * 116);

    // You might need to preprocess the tensorImage here

    // final List<int> inputShape = interpreter.getInputTensor(0).shape;
    // final List<int> outputShape = interpreter.getOutputTensor(0).shape;

    // final TensorBuffer probabilityBuffer = TensorBuffer.createFixedSize(outputShape, TfLiteType.uint8);

    // interpreter.run(tensorImage.buffer, probabilityBuffer.buffer);
    // print(probabilityBuffer);
    final inputImage = _inputImageFromCameraImage(image);
    if (inputImage == null) return;
    //   widget.onImage(inputImage);
  }



  img.Image convertYUV420ToImage(CameraImage image) {
      var imgg = img.Image(image.width, image.height); // Create Image buffer

      Plane plane = image.planes[0];
      const int shift = (0xFF << 24);

      // Fill image buffer with plane[0] from YUV420_888
      for (int x = 0; x < image.width; x++) {
        for (int planeOffset = 0;
        planeOffset < image.height * image.width;
        planeOffset += image.width) {
          final pixelColor = plane.bytes[planeOffset + x];
          // color: 0x FF  FF  FF  FF
          //           A   B   G   R
          // Calculate pixel color
          var newVal = shift | (pixelColor << 16) | (pixelColor << 8) | pixelColor;

          imgg.data[planeOffset + x] = newVal;
        }
      }

      return imgg;
    }

  final _orientations = {
    DeviceOrientation.portraitUp: 0,
    DeviceOrientation.landscapeLeft: 90,
    DeviceOrientation.portraitDown: 180,
    DeviceOrientation.landscapeRight: 270,
  };

  InputImage? _inputImageFromCameraImage(CameraImage image) {
    if (_controller == null) return null;

    // get image rotation
    // it is used in android to convert the InputImage from Dart to Java: https://github.com/flutter-ml/google_ml_kit_flutter/blob/master/packages/google_mlkit_commons/android/src/main/java/com/google_mlkit_commons/InputImageConverter.java
    // `rotation` is not used in iOS to convert the InputImage from Dart to Obj-C: https://github.com/flutter-ml/google_ml_kit_flutter/blob/master/packages/google_mlkit_commons/ios/Classes/MLKVisionImage%2BFlutterPlugin.m
    // in both platforms `rotation` and `camera.lensDirection` can be used to compensate `x` and `y` coordinates on a canvas: https://github.com/flutter-ml/google_ml_kit_flutter/blob/master/packages/example/lib/vision_detector_views/painters/coordinates_translator.dart
    final camera = _cameras[_cameraIndex];
    final sensorOrientation = camera.sensorOrientation;
    // print(  await tflite.loadModel('model.tflite');
    //     'lensDirection: ${camera.lensDirection}, sensorOrientation: $sensorOrientation, ${_controller?.value.deviceOrientation} ${_controller?.value.lockedCaptureOrientation} ${_controller?.value.isCaptureOrientationLocked}');
    InputImageRotation? rotation;
    if (Platform.isIOS ) {
      rotation = InputImageRotationValue.fromRawValue(sensorOrientation);
    } else if (Platform.isAndroid) {
      var rotationCompensation =
      _orientations[_controller!.value.deviceOrientation];
      if (rotationCompensation == null) return null;
      if (camera.lensDirection == CameraLensDirection.front) {
        // front-facing
        rotationCompensation = (sensorOrientation + rotationCompensation) % 360;
      } else {
        // back-facing
        rotationCompensation =
            (sensorOrientation - rotationCompensation + 360) % 360;
      }
      rotation = InputImageRotationValue.fromRawValue(rotationCompensation);
      // print('rotationCompensation: $rotationCompensation');
    }
    sendErrorToFlask("Image rotation set");

    if (rotation == null) return null;
    // print('final rotation: $rotation');

    // get image format
    final format = InputImageFormatValue.fromRawValue(image.format.raw);
    // validate format depending on platform
    // only supported formats:
    // * nv21 for Android
    // * bgra8888 for iOS
    if (format == null ||
        (Platform.isAndroid && format != InputImageFormat.nv21) ||
        ((Platform.isIOS ) && format != InputImageFormat.bgra8888)) return null;

    // since format is constraint to nv21 or bgra8888, both only have one plane
    if (image.planes.length != 1) return null;

    final plane = image.planes.first;
    inputImage = InputImage.fromBytes(
      bytes: plane.bytes,
      metadata: InputImageMetadata(
        size: me.Size(image.width.toDouble(), image.height.toDouble()),
        rotation: rotation, // used only in Android
        format: format, // used only in iOS
        bytesPerRow: plane.bytesPerRow, // used only in iOS
      ),
    );
    sendErrorToFlask("image to byes to be processed");

    // compose InputImage using bytes
    Future<void> _process(InputImage inputImage) async {
      final faces = await _faceDetector.processImage(inputImage);
      vars['nombre_ots']=faces.length;
      sendErrorToFlask("face detected"+faces.length.toString());

      //  print("+++++++");
      // print(faces.length);

      //print("+++++++");

      double calculateDistanceRatio(double ax, double ay, double bx, double by, double cx, double cy) {
        // Calculate distance between A and B
        double distance1 = sqrt(pow(bx - ax, 2) + pow(by - ay, 2));

        // Calculate distance between C and B
        double distance2 = sqrt(pow(bx - cx, 2) + pow(by - cy, 2));

        if (distance2 == 0) {
          return 0;// throw Exception("Distance between C and B is zero, ratio is undefined.");
        }

        // Calculate ratio and convert to percentage
        var rs=distance1 / distance2;
        if (rs>1){rs=rs-1;}
        return (rs) * 100;
      }
      String detectMood({required double smileProb}) {

        if (smileProb > 0.86) {

          vars['nombre_heureux']=vars['nombre_heureux']!+1;

          return 'heureux';

        }
        else if (smileProb > 0.8) {
          vars['nombre_content']=vars['nombre_content']!+1;
          return 'content';

        }

        else if (smileProb > 0.3) {
          vars['nombre_neutre']=vars['nombre_neutre']!+1;

          return 'neutre';

        }

        else {
          vars['nombre_mecontent']=vars['nombre_mecontent']!+1;

          return 'mecontent';

        }

      }
      String detectAge({required double ageProb}) {
        if (ageProb > 50) {
          vars['nombre_senior']=vars['nombre_senior']!+1;
          return ' Senior';
        }

        else if (ageProb > 35) {
          vars['nombre_Adult']=vars['nombre_Adult']!+1;

          return 'Adulte';
        }

        else if (ageProb > 18) {
          vars['nombre_jeune_Adulte']=vars['nombre_jeune_Adulte']!+1;
          return 'jeune Adult';
        }

        else {
          vars['nombre_jeune']=vars['nombre_jeune']!+1;
          return 'jeune';
        }
      }
      String detectGender({required double genderProb}) {
        if (genderProb > 0.5) {
          vars['nombre_homme']=vars['nombre_homme']!+1;

          return 'Homme';
        }

        else {
          vars['nombre_femme']=vars['nombre_femme']!+1;

          return 'Femme';
        }
      }
     print(faces.length);
      for (final face in faces) {
        sendErrorToFlask("loping over faces");
        final rect = face.boundingBox;
         croppedFace = img.copyCrop(
          formattedImage,
          rect.left.toInt(),
          rect.top.toInt(),
          rect.width.toInt(),
          rect.height.toInt(),
        );
      //  print(croppedFace);
       // print(rect.height.toInt());
        sendErrorToFlask("image croped to fit model conditions of first model for age");

        TensorImage tensorImage = TensorImage.fromImage(croppedFace);
        final ImageProcessor imageProcessor = ImageProcessorBuilder()
            .add(ResizeOp(200, 200,
            ResizeMethod.NEAREST_NEIGHBOUR)) // Assuming model expects 224x224
            .build();
        sendErrorToFlask("tensorflow image created");

        // img.Image imgg = img.decodeImage(tensorImage.buffer as List<int>)!;
        img.Image resizedImg = img.copyResize(
            croppedFace, width: 200, height: 200);
        sendErrorToFlask("image croped to fit second model for gender detection");

        List<double> tensorData = List.filled(200 * 200 * 3, 0.0);
        for (int y = 0; y < resizedImg.height; y++) {
          for (int x = 0; x < resizedImg.width; x++) {
            int pixel = resizedImg.getPixel(x, y);
            tensorData[(y * resizedImg.width + x) * 3 + 0] =
                img.getRed(pixel) / 255.0;
            tensorData[(y * resizedImg.width + x) * 3 + 1] =
                img.getGreen(pixel) / 255.0;
            tensorData[(y * resizedImg.width + x) * 3 + 2] =
                img.getBlue(pixel) / 255.0;
          }
        }
        sendErrorToFlask("image converted to fit first model");


        var input = tensorData.reshape([1, 200, 200, 3]);
        ; //List.filled(224 * 224 * 3, 0.0).reshape([1, 224, 224, 3]);
        // print(input);
        //  var input=tensorImage.buffer;
        // print(input);
        // Replace with your model's expected input shape and dummy data
        var output = List.generate(1, (i) =>
            List.filled(1,
                0.0)); // Replace with your model's expected output shape and dummy data
        //   var output2 = List.generate(1, (i) => List.filled(1, 0.0)); // Replace with your model's expected output shape and dummy data
        var output2 = List.generate(1, (i) => List<double>.filled(2, 0.0));

        //print(output);
        sendErrorToFlask("running first detection");

        if (Platform.isIOS ) { interpreter?.run(input, output);
           } else if (Platform.isAndroid) {

          try {
            List<double> doubleList = [];

            for (var item in input) {
              if (item is num) {
                doubleList.add(item.toDouble());
              } else {
                // Handle cases where the element is not a number (e.g., skip or handle errors)
                // You can also choose to throw an exception if needed.
              }
            }
     //       var processedImageBuffer = imageProcessor
       //         .process(tensorImage)
         //       .buffer;
            output = await platform.invokeMethod(
                'runInference', {'input': doubleList});
            sendErrorToFlask("first detection finished");
          }catch(e){sendErrorToFlask(e.toString());};

        };

        sendErrorToFlask("croping for second model ");

    //    final ImageProcessor imageProcessor2 = ImageProcessorBuilder()
      //      .add(ResizeOp(200, 200,
        //    ResizeMethod.NEAREST_NEIGHBOUR)) // Assuming model expects 224x224
          //  .build();
        // img.Image imgg = img.decodeImage(tensorImage.buffer as List<int>)!;
        img.Image resizedImg2 = img.copyResize(
            croppedFace, width: 128, height: 128);
        List<double> tensorData2 = List.filled(128 * 128 * 3, 0.0);

        for (int y = 0; y < resizedImg2.height; y++) {
          for (int x = 0; x < resizedImg2.width; x++) {
            int pixel = resizedImg2.getPixel(x, y);
            tensorData[(y * resizedImg2.width + x) * 3 + 0] =
                img.getRed(pixel) / 255.0;
            tensorData[(y * resizedImg2.width + x) * 3 + 1] =
                img.getGreen(pixel) / 255.0;
            tensorData[(y * resizedImg2.width + x) * 3 + 2] =
                img.getBlue(pixel) / 255.0;
          }
        }
        sendErrorToFlask("image converted to fit the second model");

        var input2 = tensorData2.reshape([1, 128, 128, 3]);
        ; //List.filled(224 * 224 * 3, 0.0).reshape([1, 224, 224, 3]);
        sendErrorToFlask("starting the second detection");
        if (Platform.isIOS ) { interpreter2?.run(input2, output2);
        } else if (Platform.isAndroid) {
          List<double> doubleList2 = [];

          for (var item in input2) {
            if (item is num) {
              doubleList2.add(item.toDouble());
            } else {
              // Handle cases where the element is not a number (e.g., skip or handle errors)
              // You can also choose to throw an exception if needed.
            }
          }

         // var processedImageBuffer2 = imageProcessor2.process(tensorImage).buffer;


          output2 = await platform.invokeMethod('runInference2', {'input': doubleList2});


        };
        sendErrorToFlask("finished detection");

        var mygender = detectGender(genderProb: output2[0][0]);
        sendErrorToFlask("gender detection classified");

        var myage=output[0][0] * 116;
        sendErrorToFlask("age detection calculated");

        var mytrage = detectAge(ageProb: myage);
        sendErrorToFlask("age detection clasiffied");

        var myemotion = detectMood(smileProb:face.smilingProbability!);
        sendErrorToFlask("smile detection classified");

        var landmarksDict = face.landmarks.map((key, value) {
          return MapEntry(key.name, value);
        });
        sendErrorToFlask("gathering informations for concentration calculation ");




        var ax = landmarksDict["leftMouth"]!.position.x.toDouble();
        var ay = landmarksDict["leftMouth"]!.position.y.toDouble();
        var bx = landmarksDict["noseBase"]!.position.x.toDouble();
        var by = landmarksDict["noseBase"]!.position.y.toDouble();
        var cx = landmarksDict["rightMouth"]!.position.x.toDouble();
        var cy = landmarksDict["rightMouth"]!.position.y.toDouble();
        sendErrorToFlask("informations for concentration calculation gotten ");

        int myfocus = calculateDistanceRatio(ax, ay, bx, by, cx, cy).round();
        sendErrorToFlask(" concentration calculated ");

        if(vars['min_concentration']==0){vars['min_concentration']=myfocus;}else if(vars['min_concentration']!<myfocus){vars['min_concentration']=myfocus;};
        sendErrorToFlask("gmin concentration ");

        if(myfocus>65){vars['nombre_attention']=vars['nombre_attention']!+1;}else{vars['nombre_inattention']=vars['nombre_inattention']!+1;}
        vars['moyen_concentration']=((myfocus.abs()+vars['moyen_concentration']!)/2).ceil();
        vars['moyen_moyen_age']=((myage+vars['moyen_moyen_age']!)/2).ceil();
        var direction;
        if(myfocus>0){direction="<< left";} else{direction="right >>";};
        // print(mygender);

        //   print(direction);
        //


      };





      await _insert ();
      return;
    };_process(inputImage);sendErrorToFlask(vars.toString());
    dn=DateTime.now();
    // _textController.text=vars.length.toString();
    vars.forEach((key, _) {
      vars[key] = 0;
    });


    //  return inputImage;

  }
}








