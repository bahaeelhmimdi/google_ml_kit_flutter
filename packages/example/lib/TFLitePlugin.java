import org.tensorflow.lite.Interpreter;
import java.io.File;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;



import android.os.Bundle;
import androidx.annotation.NonNull;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import org.tensorflow.lite.Interpreter;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import android.content.res.AssetFileDescriptor;
import java.io.FileInputStream;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;

import android.content.Context;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;
import java.util.ArrayList;
import java.util.List;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.embedding.engine.FlutterEngine;
import java.util.List;
public class TFLitePlugin  implements FlutterActivity   {
    private Interpreter interpreter; // The TensorFlow Lite interpreter
    private Interpreter interpreter2; // The TensorFlow Lite interpreter
	

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);
		
        initializeInterpreter("ml/model_gender_q.tflite"); // Replace with your model name
        initializeInterpreter2("ml/model_age_q.tflite"); // Replace with your model name
		
        setupChannels(flutterEngine);
        GeneratedPluginRegistrant.registerWith(flutterEngine);
		
    }

    private void initializeInterpreter(String modelName) {
        try {
            interpreter = new Interpreter(loadModelFile(modelName));
        } catch (Exception e) {
            // Handle initialization error
            e.printStackTrace();
        }
    }
    private void initializeInterpreter2(String modelName) {
        try {
            interpreter2 = new Interpreter(loadModelFile(modelName));
        } catch (Exception e) {
            // Handle initialization error
            e.printStackTrace();
        }
    }
    private MappedByteBuffer loadModelFile(String modelFileName) throws IOException {
        AssetFileDescriptor fileDescriptor = getAssets().openFd(modelFileName);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    // Function to use the initialized interpreter
    private float[][] runInference(float[] input) {
        float[][] output = new float[1][2];
        interpreter.run(input, output);
        return output;
    }
    private float[][] runInference2(float[] input) {
        float[][] output = new float[1][1];
        interpreter2.run(input, output);
        return output;
    }
    private void setupChannels(FlutterEngine flutterEngine) {
        new MethodChannel(flutterEngine.getDartExecutor(), "plugins.flutter.io/camera_android").setMethodCallHandler(
            new MethodCallHandler() {
                @Override
                public void onMethodCall(MethodCall call, Result result) {
                    if (call.method.equals("runInference")) {
                        float[] input = call.argument("input");
                        float[][] output = runInference(input);
                        result.success(output);
                    } else if(call.method.equals("runInference2")) {
                        float[] input = call.argument("input");
                        float[][] output = runInference(input);
                        result.success(output);
                    }
					else{
                        result.notImplemented();
                    	
                    }
                }
            }
        );
}}
