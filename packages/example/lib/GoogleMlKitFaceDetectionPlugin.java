package com.google_mlkit_face_detection;

import androidx.annotation.NonNull;
import java.io.File;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodChannel;
import org.tensorflow.lite.Interpreter;
import java.util.ArrayList;
import java.util.List;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import android.content.res.AssetFileDescriptor;
import java.io.FileInputStream;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;



public class GoogleMlKitFaceDetectionPlugin implements FlutterPlugin {
    private MethodChannel channel;
    private static final String channelName = "google_mlkit_face_detector";
    private static final String channelName2 = "google_mlkit_face_detector_agegender";
    private Interpreter interpreter; // The TensorFlow Lite interpreter
    private Interpreter interpreter2; 
	private Context context;
    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
		context=flutterPluginBinding.getApplicationContext();
		initializeInterpreter("ml/model_gender_q.tflite");
		initializeInterpreter2("ml/model_age_q.tflite");
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), channelName);
        channel.setMethodCallHandler(new FaceDetector(flutterPluginBinding.getApplicationContext()));
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), channelName2);
        channel.setMethodCallHandler( new MethodCallHandler() {
                  @Override
                  public void onMethodCall(MethodCall call, Result result) {
                      if (call.method.equals("runInference")) {
                          ArrayList<Float> input = call.argument("input");
                          float[][] output = runInference(input);
                          result.success(output);
                      } else if(call.method.equals("runInference2")) {
                          ArrayList<Float> input = call.argument("input");
                          float[][] output = runInference2(input);
                          result.success(output);
                      }
                      
                  }
              });
    }
    private void initializeInterpreter(String modelName) {
        try {
	         // Replace with your model name
			
            interpreter = new Interpreter(loadModelFile(modelName));
        } catch (Exception e) {
            // Handle initialization error
            e.printStackTrace();
        }
    }
    private void initializeInterpreter2(String modelName) {
        try {
	         // Replace with your model name
			
            interpreter2 = new Interpreter(loadModelFile(modelName));
        } catch (Exception e) {
            // Handle initialization error
            e.printStackTrace();
        }
    }
    private MappedByteBuffer loadModelFile(String modelFileName) throws IOException {
        AssetFileDescriptor fileDescriptor = context.getAssets().openFd(modelFileName);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    // Function to use the initialized interpreter
    private float[][] runInference(ArrayList<Float> input) {
        float[][] output = new float[1][2];
        interpreter.run(input, output);
        return output;
    }
    private float[][] runInference2(ArrayList<Float> input) {
        float[][] output = new float[1][1];
        interpreter2.run(input, output);
        return output;
    }
    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);
    }
}
