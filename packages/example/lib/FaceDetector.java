package com.google_mlkit_face_detection;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;

import androidx.annotation.NonNull;

import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceContour;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetectorOptions;
import com.google.mlkit.vision.face.FaceLandmark;
import com.google_mlkit_commons.InputImageConverter;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.common.InputImage.ImageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import org.tensorflow.lite.Interpreter;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import android.content.res.AssetFileDescriptor;
import java.io.FileInputStream;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

import android.graphics.Bitmap
import org.tensorflow.lite.support.common.ops.NormalizeOp
import org.tensorflow.lite.support.image.ImageProcessor
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.support.image.ops.ResizeOp



import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
class FaceDetector implements MethodChannel.MethodCallHandler {
    private static final String START = "vision#startFaceDetector";
    private static final String CLOSE = "vision#closeFaceDetector";

    private final Context context;
    private Interpreter interpreter; // The TensorFlow Lite interpreter
    private Interpreter interpreter2; 
    private final Map<String, com.google.mlkit.vision.face.FaceDetector> instances = new HashMap<>();

    public FaceDetector(Context context) {
        this.context = context;
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result) {
        if (call.method.equals("runInference")) {
            ArrayList<Float> input = call.argument("input");
       //     float[][] output = runInference(input);
            result.success(output);
        };
	   if(call.method.equals("runInference2")) {
            ArrayList<Float> input = call.argument("input");
        //    float[][] output = runInference2(input);
            result.success(output);
        };
        String method = call.method;
        switch (method) {
            case START:
                handleDetection(call, result);
                break;
            case CLOSE:
                closeDetector(call);
                result.success(null);
                break;
            default:
                result.notImplemented();
                break;
        }
    }
    private void initializeInterpreter(String modelName) {
        try {
	         // Replace with your model name
			
            interpreter = new Interpreter(loadModelFile(modelName));
        } catch (Exception e) {
            // Handle initialization error
            e.printStackTrace();
        }
    }
    private void initializeInterpreter2(String modelName) {
        try {
			
            interpreter2 = new Interpreter(loadModelFile(modelName));
        } catch (Exception e) {
            // Handle initialization error
            e.printStackTrace();
        }
    }
    private MappedByteBuffer loadModelFile(String modelFileName) throws IOException {
        AssetFileDescriptor fileDescriptor = getAssets().openFd(modelFileName);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }


    private void handleDetection(MethodCall call, final MethodChannel.Result result) {
        Map<String, Object> imageData = (Map<String, Object>) call.argument("imageData");
	    byte[] decodedString = Base64.decode(imageData, Base64.DEFAULT);
        var tensorInputImage = TensorImage.fromBitmap( BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length) );

		
	    private val inputImageSize = 128
		private val inputImageSize2 = 200
			
	   // var genderOutputArray = Array(1) { FloatArray(2) };
        float[][] genderOutputArray = new float[1][2];
        float[][] ageOutputArray = new float[1][1];

       
	     // Input image processor for model inputs.
	     // Resize + Normalize
	     private val inputImageProcessor =
	             ImageProcessor.Builder()
	                     .add( ResizeOp( inputImageSize , inputImageSize , ResizeOp.ResizeMethod.BILINEAR ) )
	                     .add( NormalizeOp( 0f , 255f ) )
	                     .build()
		private val inputImageProcessor2 =
				 ImageProcessor.Builder()
						                     .add( ResizeOp( inputImageSize2 , inputImageSize2 , ResizeOp.ResizeMethod.BILINEAR ) )
						                     .add( NormalizeOp( 0f , 255f ) )
						                     .build()		
		var processedImageBuffer = inputImageProcessor.process( tensorInputImage ).buffer
		var processedImageBuffer2 = inputImageProcessor2.process( tensorInputImage ).buffer

		interpreter.run(
							             processedImageBuffer,
							             genderOutputArray
							         );
 		interpreter2.run(
 							             processedImageBuffer2,
 							             ageOutputArray
 							         );		
		
		
		
		
        InputImage inputImage = InputImageConverter.getInputImageFromData(imageData, context, result);
        if (inputImage == null) return;

        String id = call.argument("id");
        com.google.mlkit.vision.face.FaceDetector detector = instances.get(id);
        if (detector == null) {
            Map<String, Object> options = call.argument("options");
            if (options == null) {
                result.error("FaceDetectorError", "Invalid options", null);
                return;
            }

            FaceDetectorOptions detectorOptions = parseOptions(options);
            detector = FaceDetection.getClient(detectorOptions);
            instances.put(id, detector);
        }

        detector.process(inputImage)
                .addOnSuccessListener(
                        visionFaces -> {
                            List<Map<String, Object>> faces = new ArrayList<>(visionFaces.size());
                            for (Face face : visionFaces) {
                                Map<String, Object> faceData = new HashMap<>();

                                Map<String, Integer> frame = new HashMap<>();
                                Rect rect = face.getBoundingBox();
                                frame.put("left", rect.left);
                                frame.put("top", rect.top);
                                frame.put("right", rect.right);
                                frame.put("bottom", rect.bottom);
                                faceData.put("rect", frame);

                                faceData.put("headEulerAngleX", face.getHeadEulerAngleX());
                                faceData.put("headEulerAngleY", face.getHeadEulerAngleY());
                                faceData.put("headEulerAngleZ", face.getHeadEulerAngleZ());
								
                                if (face.getSmilingProbability() != null) {
                                    faceData.put("smilingProbability", face.getSmilingProbability());
                                }

                                if (face.getLeftEyeOpenProbability()
                                        != null) {
                                    faceData.put("leftEyeOpenProbability", genderOutputArray[0]);
                                }

                                if (face.getRightEyeOpenProbability()
                                        != null) {
                                    faceData.put("rightEyeOpenProbability", ageOutputArray[0]*116);
                                }

                                if (face.getTrackingId() != null) {
                                    faceData.put("trackingId", Math.ceil(genderOutputArray[1]));
                                }

                                faceData.put("landmarks", getLandmarkData(face));

                                faceData.put("contours", getContourData(face));

                                faces.add(faceData);
                            }

                            result.success(faces);
                        })
                .addOnFailureListener(
                        e -> result.error("FaceDetectorError", e.toString(), null));
    }

    private FaceDetectorOptions parseOptions(Map<String, Object> options) {
		initializeInterpreter("ml/model_gender_q.tflite");
        initializeInterpreter2("ml/model_age_q.tflite"); // Replace with your model name
		
        int classification =
                (boolean) options.get("enableClassification")
                        ? FaceDetectorOptions.CLASSIFICATION_MODE_ALL
                        : FaceDetectorOptions.CLASSIFICATION_MODE_NONE;

        int landmark =
                (boolean) options.get("enableLandmarks")
                        ? FaceDetectorOptions.LANDMARK_MODE_ALL
                        : FaceDetectorOptions.LANDMARK_MODE_NONE;

        int contours =
                (boolean) options.get("enableContours")
                        ? FaceDetectorOptions.CONTOUR_MODE_ALL
                        : FaceDetectorOptions.CONTOUR_MODE_NONE;

        int mode;
        switch ((String) options.get("mode")) {
            case "accurate":
                mode = FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE;
                break;
            case "fast":
                mode = FaceDetectorOptions.PERFORMANCE_MODE_FAST;
                break;
            default:
                throw new IllegalArgumentException("Not a mode:" + options.get("mode"));
        }

        FaceDetectorOptions.Builder builder =
                new FaceDetectorOptions.Builder()
                        .setClassificationMode(classification)
                        .setLandmarkMode(landmark)
                        .setContourMode(contours)
                        .setMinFaceSize((float) ((double) options.get("minFaceSize")))
                        .setPerformanceMode(mode);

        if ((boolean) options.get("enableTracking")) {
            builder.enableTracking();
        }

        return builder.build();
    }

    private Map<String, double[]> getLandmarkData(Face face) {
        Map<String, double[]> landmarks = new HashMap<>();

        landmarks.put("bottomMouth", landmarkPosition(face, FaceLandmark.MOUTH_BOTTOM));
        landmarks.put("rightMouth", landmarkPosition(face, FaceLandmark.MOUTH_RIGHT));
        landmarks.put("leftMouth", landmarkPosition(face, FaceLandmark.MOUTH_LEFT));
        landmarks.put("rightEye", landmarkPosition(face, FaceLandmark.RIGHT_EYE));
        landmarks.put("leftEye", landmarkPosition(face, FaceLandmark.LEFT_EYE));
        landmarks.put("rightEar", landmarkPosition(face, FaceLandmark.RIGHT_EAR));
        landmarks.put("leftEar", landmarkPosition(face, FaceLandmark.LEFT_EAR));
        landmarks.put("rightCheek", landmarkPosition(face, FaceLandmark.RIGHT_CHEEK));
        landmarks.put("leftCheek", landmarkPosition(face, FaceLandmark.LEFT_CHEEK));
        landmarks.put("noseBase", landmarkPosition(face, FaceLandmark.NOSE_BASE));

        return landmarks;
    }

    private Map<String, List<double[]>> getContourData(Face face) {
        Map<String, List<double[]>> contours = new HashMap<>();

        contours.put("face", contourPosition(face, FaceContour.FACE));
        contours.put(
                "leftEyebrowTop", contourPosition(face, FaceContour.LEFT_EYEBROW_TOP));
        contours.put(
                "leftEyebrowBottom", contourPosition(face, FaceContour.LEFT_EYEBROW_BOTTOM));
        contours.put(
                "rightEyebrowTop", contourPosition(face, FaceContour.RIGHT_EYEBROW_TOP));
        contours.put(
                "rightEyebrowBottom",
                contourPosition(face, FaceContour.RIGHT_EYEBROW_BOTTOM));
        contours.put("leftEye", contourPosition(face, FaceContour.LEFT_EYE));
        contours.put("rightEye", contourPosition(face, FaceContour.RIGHT_EYE));
        contours.put("upperLipTop", contourPosition(face, FaceContour.UPPER_LIP_TOP));
        contours.put(
                "upperLipBottom", contourPosition(face, FaceContour.UPPER_LIP_BOTTOM));
        contours.put("lowerLipTop", contourPosition(face, FaceContour.LOWER_LIP_TOP));
        contours.put(
                "lowerLipBottom", contourPosition(face, FaceContour.LOWER_LIP_BOTTOM));
        contours.put("noseBridge", contourPosition(face, FaceContour.NOSE_BRIDGE));
        contours.put("noseBottom", contourPosition(face, FaceContour.NOSE_BOTTOM));
        contours.put("leftCheek", contourPosition(face, FaceContour.LEFT_CHEEK));
        contours.put("rightCheek", contourPosition(face, FaceContour.RIGHT_CHEEK));

        return contours;
    }

    private double[] landmarkPosition(Face face, int landmarkInt) {
        FaceLandmark landmark = face.getLandmark(landmarkInt);
        if (landmark != null) {
            return new double[]{landmark.getPosition().x, landmark.getPosition().y};
        }
        return null;
    }

    private List<double[]> contourPosition(Face face, int contourInt) {
        FaceContour contour = face.getContour(contourInt);
        if (contour != null) {
            List<PointF> contourPoints = contour.getPoints();
            List<double[]> result = new ArrayList<>();
            for (int i = 0; i < contourPoints.size(); i++) {
                result.add(new double[]{contourPoints.get(i).x, contourPoints.get(i).y});
            }
            return result;
        }
        return null;
    }

    private void closeDetector(MethodCall call) {
        String id = call.argument("id");
        com.google.mlkit.vision.face.FaceDetector detector = instances.get(id);
        if (detector == null) return;
        detector.close();
        instances.remove(id);
    }
}
