import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
//import 'package:intl/intl.dart';

class DatabaseHelper {
  static final _databaseName = "MyDatabase5.db";
  static final _databaseNamec = "MyDatabasec.db";

  static final _databaseVersion = 1;

  static final table = 'qias_table';
  static final tablec = 'qias_tablec';

  static final columnId = '_id';
  static final columnName = 'name';

  static final column_nombre_senior = 'nombre_senior';
  static final column_nombre_Adult = 'nombre_Adult';
  static final column_nombre_jeune_Adulte = 'nombre_jeune_Adulte';
  static final column_nombre_jeune = 'nombre_jeune';
  static final column_nombre_moyen_age = 'nombre_moyen_age';



  static final column_moyen_senior = 'moyen_senior';
  static final column_moyen_Adult = 'moyen_Adult';
  static final column_moyen_jeune_Adulte = 'moyen_jeune_Adulte';
  static final column_moyen_jeune = 'moyen_jeune';
  static final column_moyen_moyen_age = 'moyen_moyen_age';


  static final column_nombre_homme = 'nombre_homme';
  static final column_nombre_femme = 'nombre_femme';

  static final column_moyen_homme = 'moyen_homme';
  static final column_moyen_femme = 'moyen_femme';




  static final column_nombre_heureux = 'nombre_heureux';
  static final column_nombre_content = 'nombre_content';
  static final column_nombre_neutre = 'nombre_neutre';
  static final column_nombre_mecontent = 'nombre_mecontent';

  static final column_moyen_heureux = 'moyen_heureux';
  static final column_moyen_content = 'moyen_content';
  static final column_moyen_neutre = 'moyen_neutre';
  static final column_moyen_mecontent = 'moyen_mecontent';



  static final column_min_concentration = 'min_concentration';
  static final column_moyen_concentration = 'moyen_concentration';

  static final column_nombre_temps_attention = 'nombre_temps_attention';
  static final column_nombre_temps_inattention = 'nombre_temps_inattention';


  static final column_nombre_ots = 'nombre_ots';
  static final column_nombre_attention = 'nombre_attention';
  static final column_nombre_inattention = 'nombre_inattention';
  static final column_moyen_attention = 'moyen_attention';
  static final column_moyen_inattention = 'moyen_inattention';
  static final columnDate = 'thedate';
  static final columntime = 'thetime';

  static final columnDatetime = 'datetime';
  static final columnNomAppareil = 'nom_appareil';
 // static final columnDirection = 'direction';
//////////////////////////////////////////////////////////////
  static final columnId2 = '_id';
  static final column_cumul_attention = 'cumul_attention';
  static final column_cumul_inattention = 'cumul_inattention';
  static final column_cumul_temps_attention = 'cumul_temps_attention';
  static final column_cumul_temps_inattention = 'cumul_temps_inattention';
  static final column_cumul_ots = 'cumul_ots';
  static final column_cumul_temps_ots = 'cumul_temps_ots';

  static final column_cumul_senior = 'cumul_senior';
  static final column_cumul_Adult = 'cumul_Adult';
  static final column_cumul_jeune_Adulte = 'cumul_jeune_Adulte';
  static final column_cumul_jeune = 'cumul_jeune';
  static final column_cumul_moyen_age = 'cumul_moyen_age';

  static final column_cumul_heureux = 'cumul_heureux';
  static final column_cumul_content = 'cumul_content';
  static final column_cumul_neutre = 'cumul_neutre';
  static final column_cumul_mecontent = 'cumul_mecontent';

  static final column_cumul_homme = 'cumul_homme';
  static final column_cumul_femme = 'cumul_femme';
  // make this a singleton class
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // only have a single app-wide reference to the database
  static Map? _cumuls;

  static Database? _database;
  static Database? _databasec;
  static Batch? _batch;

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDatabase();
    return _database!;
  }
  Future<Database> get databasec async {
    if (_databasec != null) return _databasec!;
    _databasec = await _initDatabasec();
    return _databasec!;
  }
  Future<Batch> get batch async {
    if (_batch != null) return _batch!;
    Database db = await instance.database;

    _batch = await db.batch();
    return _batch!;
  }
  Future<Map> get cumuls async {
    if (_cumuls != null) return _cumuls!;

    var got=  await queryAllRowsc();;
    if (got.length==0){
      Database dbc = await instance.databasec;
      Map<String, dynamic> row = {
        DatabaseHelper.column_cumul_attention: 0,
        DatabaseHelper.column_cumul_inattention: 0,
        DatabaseHelper.column_cumul_ots: 0,
        DatabaseHelper.column_cumul_temps_attention: 0,
        DatabaseHelper.column_cumul_temps_inattention:0,
        DatabaseHelper.column_cumul_temps_ots:0,
       DatabaseHelper.column_cumul_temps_ots:0,

       DatabaseHelper.column_cumul_senior  :0,
       DatabaseHelper.column_cumul_Adult  :0,
       DatabaseHelper.column_cumul_jeune_Adulte  :0,
       DatabaseHelper.column_cumul_jeune  :0,
       DatabaseHelper.column_cumul_moyen_age  :0,

       DatabaseHelper.column_cumul_heureux  :0,
       DatabaseHelper.column_cumul_content  :0,
       DatabaseHelper.column_cumul_neutre  :0,
       DatabaseHelper.column_cumul_mecontent  :0,

       DatabaseHelper.column_cumul_homme  :0,
       DatabaseHelper.column_cumul_femme  :0,
    };

      try{dbc.insert(tablec, row);return row;}
      catch(e){print(e);}
    };

    return got.first;
  }
  _initDatabase() async {
    var documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }
  _initDatabasec() async {
    var documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseNamec);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreatec);
  }
  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $table (
          $columnId INTEGER PRIMARY KEY,

          $column_nombre_senior   INTEGER NOT NULL,
          $column_nombre_Adult   INTEGER NOT NULL,
          $column_nombre_jeune_Adulte   INTEGER NOT NULL,
          $column_nombre_jeune  INTEGER NOT NULL,
          $column_nombre_moyen_age   INTEGER NOT NULL,

          $column_moyen_senior   INTEGER NOT NULL,
          $column_moyen_Adult   INTEGER NOT NULL,
          $column_moyen_jeune_Adulte   INTEGER NOT NULL,
          $column_moyen_jeune   INTEGER NOT NULL,
          $column_moyen_moyen_age   INTEGER NOT NULL,


          $column_nombre_homme   INTEGER NOT NULL,
          $column_nombre_femme   INTEGER NOT NULL,

          $column_moyen_homme   INTEGER NOT NULL,
          $column_moyen_femme   INTEGER NOT NULL,


          $column_nombre_heureux   INTEGER NOT NULL,
          $column_nombre_content   INTEGER NOT NULL,
          $column_nombre_neutre   INTEGER NOT NULL,
          $column_nombre_mecontent   INTEGER NOT NULL,

          $column_moyen_heureux   INTEGER NOT NULL,
          $column_moyen_content   INTEGER NOT NULL,
          $column_moyen_neutre   INTEGER NOT NULL,
          $column_moyen_mecontent   INTEGER NOT NULL,

          $column_min_concentration   INTEGER NOT NULL,
          $column_moyen_concentration   INTEGER NOT NULL,

          $column_nombre_temps_attention   INTEGER NOT NULL,
          $column_nombre_temps_inattention   INTEGER NOT NULL,
          $column_nombre_ots   INTEGER NOT NULL,
          $column_nombre_attention  INTEGER NOT NULL,
          $column_nombre_inattention   INTEGER NOT NULL,
          $column_moyen_attention  INTEGER NOT NULL,
          $column_moyen_inattention   INTEGER NOT NULL,


          $columnNomAppareil   TEXT NOT NULL,
          $columnDate TEXT NOT NULL,
          $columntime TEXT NOT NULL,

          $columnDatetime DATETIME,
          $column_cumul_attention INTEGER NOT NULL,
          $column_cumul_inattention INTEGER NOT NULL,
          $column_cumul_ots INTEGER NOT NULL,          
          $column_cumul_temps_attention INTEGER NOT NULL,
          $column_cumul_temps_inattention INTEGER NOT NULL,
          $column_cumul_temps_ots INTEGER NOT NULL,
          
          
          $column_cumul_homme   INTEGER NOT NULL,
          $column_cumul_femme   INTEGER NOT NULL,


          $column_cumul_senior   INTEGER NOT NULL,
          $column_cumul_Adult   INTEGER NOT NULL,
          $column_cumul_jeune_Adulte   INTEGER NOT NULL,
          $column_cumul_jeune  INTEGER NOT NULL,
          $column_cumul_moyen_age   INTEGER NOT NULL,


          $column_cumul_heureux   INTEGER NOT NULL,
          $column_cumul_content   INTEGER NOT NULL,
          $column_cumul_neutre   INTEGER NOT NULL,
          $column_cumul_mecontent   INTEGER NOT NULL

          )
          ''');
  }
  Future _onCreatec(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $table (
          $columnId2 INTEGER PRIMARY KEY,
          $column_cumul_attention INTEGER NOT NULL,
          $column_cumul_inattention INTEGER NOT NULL,
          $column_cumul_ots INTEGER NOT NULL,          
          $column_cumul_temps_attention INTEGER NOT NULL,
          $column_cumul_temps_inattention INTEGER NOT NULL,
          $column_cumul_temps_ots INTEGER NOT NULL,

          )
          ''');
  }

  Future<int> insert(Map<String, dynamic> row) async {
    Batch batch = await instance.batch;


      try{batch.insert(table, row);return  1;}
      catch(e){print(e);return 0;}

  }

  Future<void> commit() async {
    Database db = await instance.database;
    Batch batch = await instance.batch;
   var res=await batch.commit();
  }

  Future<List<Map<String, dynamic>>> queryAllRowsd() async {
    Database db = await instance.database;
    return await db.transaction((txn) async {
      var qs=await txn.query(table);
      await txn.rawDelete('DELETE FROM $table');
      return qs;

    });
  }
  Future<List<Map<String, dynamic>>> queryAllRows() async {
    Database db = await instance.database;
    return await db.transaction((txn) async {
      var qs=await txn.query(table);
      return qs;

    });
  }
  Future<List<Map<String, dynamic>>> queryAllRowsc() async {
    Database db = await instance.databasec;
    return await db.transaction((txn) async {
      return await txn.query(tablec);


    });
  }
  Future<Map> update(Map<String, dynamic> row) async {
    Database databasec = await instance.databasec;
    Map cumuls = await instance.cumuls;
    Map<String, int> resultMap = row.map((key, value) => MapEntry(key, value + cumuls[key]!));


    await databasec.insert(
      tablec,
      resultMap,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
   return resultMap;
  }

}
