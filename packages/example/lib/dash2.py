

<!DOCTYPE html>
<html>
<head>
    <title>Pie Chart Generator</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
</head>
  <style>
        body {
            margin: 0;
            padding: 0;
        }

        .grid-container {
            display: grid;
            grid-template-columns: repeat(3, 510px);
            grid-template-rows: repeat(2, 430px);
        }

        .grid-item {
            width: 100%;
            height: 100%;
        }

        iframe {
            width: 100%;
            height: 100%;
            border: none;
        }
             .centered-text {
            text-align: center;
        }
        .centered-text-right {
            text-align: right;
        }
        .centered-text-left {
            text-align: left;
        }

        .bold-text {
            font-weight: bold;
        }

        .smaller-text {
            font-size: 0.8em; /* You can adjust the size as needed */
        }
    </style>
      <style>
    @font-face {
      font-family: 'popin';
      src: url('/static/p.ttf') format('ttf'); /* Adjust the path and format accordingly */
      /* You can include additional font formats here, such as woff, ttf, etc., for better browser compatibility */
    }

    body {
      font-family: 'popin', sans-serif;
      /* Additional styling for the body */
    }

    /* Additional styling for other elements if needed */
  </style>
    <style>
        /* Style for the container */
        .container {
            display: flex; /* Use flexbox for layout */
            justify-content: space-between; /* Add space between the two containers */
              gap: 1px; /* Adjust the value to change the space between items */

            /* Add margin for additional space around the containers */
        }

        /* Style for each rectangle */
        .rectangle {
            width: 46vw; /* Set the width of each rectangle */
            height: 100px; /* Set the height of each rectangle */
            background-color: rgba(65,164,255,255); /* Set the background color to blue */
            color: white; /* Set the text color to white */
            text-align: center; /* Center-align text */
            line-height: 100px; /* Set line height equal to rectangle height for vertical centering */
        }
                .rectangle2 {
            width: 46vw; /* Set the width of each rectangle */
            height: 100px; /* Set the height of each rectangle */
            background-color: white; /* Set the background color to blue */
            color: black; /* Set the text color to white */
            text-align: center; /* Center-align text */
            line-height: 1px; /* Set line height equal to rectangle height for vertical centering */
        }
             .rectangle3 {
            width: 46vw; /* Set the width of each rectangle */
            height: 100px; /* Set the height of each rectangle */
            background-color: white; /* Set the background color to blue */
            color: black; /* Set the text color to white */
            text-align: center; /* Center-align text */
            line-height: 1px; /* Set line height equal to rectangle height for vertical centering */
        }
                     .rectangle4 {
            width: 20vw; /* Set the width of each rectangle */
            height: 20px; /* Set the height of each rectangle */
            background-color: rgba(65,164,255,255); /* Set the background color to blue */
            color: white; /* Set the text color to white */
            text-align: center; /* Center-align text */
            line-height: 100px;
        }
        #yourDivId .main-title {
  background-color: lightblue; /* Set the background color for the title */
  padding: 10px; /* Adjust padding as needed */
}
    </style>
<body>
    <h1>Pie Chart Generator</h1>
    <form action="/generate_chart" method="POST">
        <label for="start_date">Start Date and Time:</label>
        <input type="text" id="start_date" name="start_date" readonly><br><br>
        <label for="end_date">End Date and Time:</label>
        <input type="text" id="end_date" name="end_date" readonly><br><br>
        <input  type="text"  name="url" id="url" readonly><br><br>

    </form>

    <div class="grid-container" id="iframeContainer"></div>

     <ul>
     <input type="text" id="search-bar" placeholder="Search buttons by text">

 <table>
            <tr>
                {% for column in columns %}
                    <td >
                        <li class="button-item"><button type="submit" name="column" value="{{ column }}" class="btn red">[{{ column }}]</button></li>
                    </td>
                    {% if loop.index % 5 == 0 %}
                        </tr><tr>
                    {% endif %}
                {% endfor %}
            </tr>
        </table>
        <button  onclick="createIframe()">go</button>
          <div class="container">
        <!-- First rectangle -->

        <div class="rectangle" style=" margin-left: 20px;">
            Attention
        </div>

        <!-- Second rectangle -->
        <div class="rectangle" style=" margin-right: 50px;">
            Conversion
        </div>
    </div>
    <div class="container">
<div class="rectangle2" style=" margin-left: 20px;">

           <div class="centered-text-left">
        <p class="bold-text"><iframe src="https://qias.pythonanywhere.com/generate_text?start_date=2023-10-02%2012:00&end_date=2023-10-11%2012:00&columns=%27[%27nombre_temps_attention%27,%27nombre_temps_inattention%27]%27" frameborder="0" style="border: none; width: 40px; height: 40px;text-align: left;"></iframe>
</p>
        <p class="smaller-text">Your Smaller Text Here</p>
    </div>

       <div class="centered-text-right">
        <p class="bold-text">Your Bold Text Here</p>
        <p class="smaller-text">Your Smaller Text Here</p>
    </div>

    <div class="centered-text">
        <p class="bold-text">Your Bold Text Here</p>
        <p class="smaller-text">Your Smaller Text Here</p>
    </div>

        </div>
  <div class="rectangle2" style=" margin-right: 50px;">
           <div class="centered-text-left">
        <p class="bold-text">Your Bold Text Here</p>
        <p class="smaller-text">Your Smaller Text Here</p>
    </div>

       <div class="centered-text-right">
        <p class="bold-text">Your Bold Text Here</p>
        <p class="smaller-text">Your Smaller Text Here</p>
    </div>
<div class="centered-text">
        <p class="bold-text">Your Bold Text Here</p>
        <p class="smaller-text">Your Smaller Text Here</p>
    </div>
        </div>




        </div>
<div class="countainer">


<iframe src="https://qias.pythonanywhere.com/generate_chart?start_date=2023-10-02%2012:00&end_date=2023-10-04%2012:00&columns=%27[%27taux_conversion_senior%27,%27taux_conversion_Adult%27,%27taux_conversion_jeune_Adult%27,%27taux_conversion_jeune%27]%27" frameborder="0" style="border: none; width: 500px; height: 500px;text-align: left;"></iframe>
       <iframe  src="https://qias.pythonanywhere.com/barchart_ratio?start_date=2023-10-03%2012:00&end_date=2023-10-12%2012:00&columns=%27[%27nombre_temps_attention_homme%27,%27nombre_temps_attention_femme%27]%27" frameborder="0" style="border: none; width: 300px; height: 500px;text-align: right;"></iframe>


<iframe src="https://qias.pythonanywhere.com/generate_chart?start_date=2023-10-02%2012:00&end_date=2023-10-04%2012:00&columns=%27[%27taux_conversion_senior%27,%27taux_conversion_Adult%27,%27taux_conversion_jeune_Adult%27,%27taux_conversion_jeune%27]%27" frameborder="0" style="border: none; width: 500px; height: 500px;text-align: left;"></iframe>
       <iframe  src="https://qias.pythonanywhere.com/barchart_ratio?start_date=2023-10-03%2012:00&end_date=2023-10-12%2012:00&columns=%27[%27nombre_temps_attention_homme%27,%27nombre_temps_attention_femme%27]%27" frameborder="0" style="border: none; width: 300px; height: 500px;text-align: right;"></iframe>

       <div class="container">
        <!-- First rectangle -->

        <div class="rectangle4" style=" margin-left: 20px;width:40vw;z-index">
            Attention
            <iframe src="https://qias.pythonanywhere.com/barchart_ratio2?start_date=2023-10-03%2012:00&end_date=2023-10-08%2012:00&columns=%27%5B%27nombre_senior_homme%27,%27nombre_Adult_homme%27,%27nombre_jeune_Adult_homme%27,%27nombre_jeune_homme%27%5D%27" frameborder="0" style="border: none; width: 300px; height: 470px;position:absolute;left:1vw;top:200vh"></iframe>
       <iframe  src="https://qias.pythonanywhere.com/barchart_ratio2?start_date=2023-10-03%2012:00&end_date=2023-10-08%2012:00&columns=%27%5B%27nombre_senior_homme%27,%27nombre_Adult_homme%27,%27nombre_jeune_Adult_homme%27,%27nombre_jeune_homme%27%5D%27" frameborder="0" style="border: none; width: 300px; height: 470px;position:absolute;left:20vw;top:200vh"></iframe>

        </div>

        <!-- Second rectangle -->
        <div class="rectangle4" style=" margin-right: 50px;">
            Conversion
        </div>
           <div class="rectangle4" style=" margin-right: 50px;">
            Conversion
        </div>
    </div>
           <div class="container">


</div>
</div>

<script>
        const searchBar = document.getElementById('search-bar');
        const buttonList = document.querySelectorAll('.button-item');

        searchBar.addEventListener('input', () => {
            const searchText = searchBar.value.toLowerCase();

            buttonList.forEach((buttonItem) => {
                const button = buttonItem.querySelector('button');
                const buttonText = button.textContent.toLowerCase();

                if (buttonText.includes(searchText)) {
                    buttonItem.style.display = 'block';
                } else {
                    buttonItem.style.display = 'none';
                }
            });
        });
    </script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>


    <script>


    // Initialize Flatpickr for date and time selection with disabled ranges
    flatpickr("#start_date", {
        enableTime: true,
        min: "{{ min_date }}",
        max: "{{ max_date }}",
        dateFormat: "Y-m-d H:i",
        disable: [
           function(date) {
    var minDate = new Date("{{ min_date }}");
    minDate.setDate(minDate.getDate() + 1); // Add a day
    return (date > minDate);
            }
        ]
    });

    flatpickr("#end_date", {
        enableTime: true,
        min: "{{ min_date }}",
        max: "{{ max_date }}",
        dateFormat: "Y-m-d H:i",
        disable: [

            function(date) {
    var maxDate = new Date("{{ max_date }}");
    maxDate.setDate(maxDate.getDate() - 1); // Subtract a day
    return (date < maxDate);            }
        ]
    });

function generateChart() {
    var start_date = document.getElementById("start_date").value;
    var end_date = document.getElementById("end_date").value;
    console.log("searching");
    if (start_date !== "" && end_date !== "") {
        var greenButtons = document.querySelectorAll('.green');

// Initialize an array to store the values or names
var buttonValuesOrNames = [];
// Loop through the NodeList and extract the values or names
greenButtons.forEach(function(button) {
  // If the button has a "value" attribute, add it to the array
  if (button.hasAttribute('value')) {
    buttonValuesOrNames.push("'"+button.getAttribute('value')+"'");
  }
  // If the button has a "name" attribute, add it to the array
  else if (button.hasAttribute('name')) {
    buttonValuesOrNames.push(button.getAttribute('name'));
  }
});
console.log(buttonValuesOrNames.toString());
        //    var iframe = document.getElementById("my-iframe");
        //     iframe.src = "";
           // iframe.src
            document.getElementById('url').value= "/generate_chart?start_date="+start_date+"&end_date="+end_date+"&columns='['"+buttonValuesOrNames.toString().slice(1,-1)+"']'";
    }
}
 function createIframe(url) {
      // Create a new iframe element
      var iframe = document.createElement('iframe');

      // Set attributes for the iframe
      iframe.src = url; // Set the source URL
      iframe.width = '500'; // Set the width
      if (url.includes("text")){iframe.height = '100';}else{iframe.height = '500';}
       // Set the height
  //    ifrmae.id="";
      iframe.style.border = "0"; // Remove the iframe border
     iframe.addEventListener("click", function() {
    // Change the border color to green
  //  iframe.frameborder = '1'
    iframe.style.borderColor = "green";
});
      // Append the iframe to the div
      var iframeContainer = document.getElementById('iframeContainer');



        var newDiv = document.createElement("div");

    // Create "Select" button
    var selectButton = document.createElement("button");
    selectButton.textContent = "Select";
    selectButton.onclick = function() {
        Array.from(document.getElementById('iframeContainer').getElementsByTagName('div')).forEach(function(item) {
  item.style.backgroundColor = "white";
});
        newDiv.style.backgroundColor = "lightblue";
        ifr=Array.from(newDiv.getElementsByTagName('iframe'))[0]
             ifr.src = "";
            ifr.src =url;

    };


    // Create "Remove" button
    var removeButton = document.createElement("button");
    removeButton.textContent = "Remove";
    removeButton.onclick = function() {
        newDiv.parentNode.removeChild(newDiv);
    };

    // Append buttons to the new div
    newDiv.appendChild(selectButton);
    newDiv.appendChild(removeButton);
        newDiv.appendChild(iframe);

    // Append the new div to the body or any other desired parent element
    iframeContainer.appendChild(newDiv);
    }

    // Initial chart generation
    generateChart();
 const urls = [
     'https://qias.pythonanywhere.com/generate_text?start_date=2023-10-02%2012:00&end_date=2023-10-04%2012:00&columns=%27[%27nombre_ots%27]%27',
'https://qias.pythonanywhere.com/generate_text_ratio?start_date=2023-10-02%2012:00&end_date=2023-10-05%2012:00&columns=%27[%27nombre_attention%27,%27nombre_inattention%27]%27',
"https://qias.pythonanywhere.com/generate_text_ratio?start_date=2023-10-02%2012:00&end_date=2023-10-05%2012:00&columns=%27[%27nombre_temps_attention%27,%27nombre_temps_inattention%27]%27",

     'https://qias.pythonanywhere.com/generate_chart?start_date=2023-10-02%2012:00&end_date=2023-10-04%2012:00&columns=%27[%27taux_conversion_senior%27,%27taux_conversion_Adult%27,%27taux_conversion_jeune_Adult%27,%27taux_conversion_jeune%27]%27',
            'https://qias.pythonanywhere.com/generate_chart?start_date=2023-10-02%2012:00&end_date=2023-10-04%2012:00&columns=%27[%27nombre_temps_attention_senior%27,%27nombre_temps_attention_Adult%27,%27nombre_temps_attention_jeune_Adult%27,%27nombre_temps_attention_jeune%27]%27',
                 'https://qias.pythonanywhere.com/generate_chart?start_date=2023-10-02%2012:00&end_date=2023-10-04%2012:00&columns=%27[%27nombre_senior%27,%27nombre_Adult%27,%27nombre_jeune_Adult%27,%27nombre_jeune%27]%27',
            'https://qias.pythonanywhere.com/generate_chart?start_date=2023-10-02%2012:00&end_date=2023-10-04%2012:00&columns=%27[%27nombre_homme%27,%27nombre_femme%27]%27',
            'https://qias.pythonanywhere.com/barchart?start_date=2023-10-03%2012:00&end_date=2023-10-12%2012:00&columns=%27[%27nombre_temps_attention_homme%27,%27nombre_temps_attention_femme%27]%27'

        ];

        // Call createIframe function for each URL
        for (let i = 0; i < urls.length; i++) {
            createIframe(urls[i]);
        }
    // Refresh the chart every 60 seconds
   // setInterval(generateChart, 6000);
</script>

</body>
</html>

