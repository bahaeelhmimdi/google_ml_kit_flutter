
import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as shelf_io;

Future<void> startServer() async {
  final handler = const Pipeline().addMiddleware(logRequests()).addHandler(_echoRequest);

  final server = await shelf_io.serve(handler, 'localhost', 8080);
  print('Server running on http://${server.address.host}:${server.port}');
}

Response _echoRequest(Request request) {
  final htmlContent = '''
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Automatic Camera Capture</title>
</head>
<body>
 <script src="https://stephenlb.github.io/spoken/spoken.js"></script>
      <a id="error-container">test</a>
       <a hidden id="speak">notspeaking</a>
    <video hidden id="video" width="640" height="480" autoplay></video>
    <canvas id="canvas" width="640" height="480" ></canvas>
    <script>
    function showError(message) {
      var errorContainer = document.getElementById('error-container');
      errorContainer.textContent = message;
      errorContainer.style.color = 'red'; // Optional: color the error message
    }
       const video = document.getElementById('video');
                const canvas = document.getElementById('canvas');
                var speaking=false;
        async function startCamera() {
            try {
             
                const context = canvas.getContext('2d');

                // Request video stream from the camera
               // const stream = await navigator.mediaDevices.getUserMedia({ video: true });
              //  video.srcObject = stream;
 if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Request access to the camera
        navigator.mediaDevices.getUserMedia({ video: { 
        width: { min: 640 },
        height: { min: 480 }
    } })
            .then(function(stream) {
                // Set the source of the video element to the stream from the camera
                video.srcObject = stream;
                video.play();
                showError("video played")
                     video.addEventListener('loadeddata', () => {
                    setInterval(() => {
                        context.drawImage(video, 0, 0, canvas.width, canvas.height);
                        const dataUrl = canvas.toDataURL('image/jpeg');

                        // Send the image data URL to Flutter
                        if (window.flutter && window.flutter.postMessage) {
                            window.flutter.postMessage(dataUrl);
                        }
                    }, 1000); // Capture an image every second
                });
          })
          
            .catch(function(err) {
                showError("Error accessing the camera: " + err.message);
              startCamera();
            });
    } else {
       showError("getUserMedia is not supported by this browser.");
    }
                // Continuously capture frames from the video
           
            } catch (err) {
                showError( err.message);
                 startCamera();
            }
        }

        // Start the camera automatically
        document.addEventListener('DOMContentLoaded',  () => {
        startCamera();
;})
        
    </script>
</body>
</html>
  ''';

  return Response.ok(htmlContent, headers: {'Content-Type': 'text/html'});
}
