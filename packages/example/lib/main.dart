import 'dart:ffi' ;
import 'dart:io';
import 'dart:ui' as me;
import 'dart:ui';
import 'dart:developer' as developer;

import 'package:flutter/foundation.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tflite_flutter/tflite_flutter.dart';
//import 'package:tflite_flutter_helper/tflite_flutter_helper.dart';
import 'dart:typed_data';  // You'll need this for Float32List
import 'package:image/image.dart' as img;
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';
import 'database_helper.dart';
import 'dart:math';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:device_info_plus/device_info_plus.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:flutter_background_service_android/flutter_background_service_android.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
//import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter/services.dart'; // For platform-specific code
import 'package:flutter/services.dart';
import 'dart:developer' as developer;
import 'package:logging/logging.dart';
import 'dart:io';
import 'dart:async';



//////
import 'dart:ffi' ;
import 'dart:io';
import 'dart:isolate';
import 'dart:ui' as me;
import 'dart:ui';
import 'dart:developer' as developer;
import 'package:dio/io.dart';

import 'server.dart';
import 'package:flutter/foundation.dart';
//import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tflite_flutter/tflite_flutter.dart';
//import 'package:tflite_flutter_helper/tflite_flutter_helper.dart';
import 'dart:typed_data';  // You'll need this for Float32List
import 'package:image/image.dart' as img;
import 'package:google_mlkit_face_detection/google_mlkit_face_detection.dart';
import 'database_helper.dart';
import 'dart:math';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:device_info_plus/device_info_plus.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:flutter_background_service_android/flutter_background_service_android.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
//import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter/services.dart'; // For platform-specific code
import 'package:flutter/services.dart';
import 'dart:developer' as developer;
import 'package:logging/logging.dart';
import 'dart:io';
import 'dart:async';
//import 'package:webview_flutter/webview_flutter.dart';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:webview_flutter_platform_interface/webview_flutter_platform_interface.dart';

import 'dart:async';

import 'dart:convert';

import 'dart:typed_data';

// Import for Android features.
import 'package:webview_flutter_android/webview_flutter_android.dart';
// Import for iOS features.
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';
import 'server.dart';


import 'dart:async';


import 'dart:io';
import 'package:dio/dio.dart';
import 'dart:typed_data';
import 'package:image/image.dart' as img;


import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart' show rootBundle;

import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';

import 'dart:io';
import 'package:dio/dio.dart';
import 'package:video_player/video_player.dart';
import 'dart:convert';
import 'package:camera/camera.dart';
//////
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

const String AUTH_URL = "https://qias.itechiatv.com/qiastestv2/users/authBox";
const String SEND_DETECTION_URL = "https://qias.itechiatv.com/qiastestv2/impressions";
const String REFRESH_TOKEN_URL = "https://qias.itechiatv.com/qiastestv2/authBox";

// Paramètres d'authentification (Méthode 1)
const String MAC_ADDRESS = "f4:d4:88:7e:d5:65";
const String BOX_SECRET_KEY = r'b8A6U!G7HnG$%KMuLQ&5KbMf!UyqP*';
var detections = [];
var ms= [];
String accessToken = '';
String refreshToken = '';
String validFrom = '';
String validTo = '';

// Fonction pour obtenir l'accessToken
Future<void> authenticateDevice() async {


  final headers = {'Content-Type': 'application/json'};
  final body = jsonEncode({
    "adressMac": MAC_ADDRESS,
    "boxSecretKey": BOX_SECRET_KEY,
    "withRefreshToken": true,
    "grantType": "password",
  });

  final response = await http.post(Uri.parse(AUTH_URL), headers: headers, body: body);

  if (response.statusCode == 200) {
    final data = jsonDecode(response.body);
    accessToken = data['accessToken'];
    refreshToken = data['refreshToken'];
    validFrom = data['validFrom'];
    validTo = data['validTo'];

    print("Authentification réussie.");
    print("accessToken: $accessToken");
    print("refreshToken: $refreshToken");
    print("Licence valide du: $validFrom au: $validTo");
  } else {
    print("Erreur d'authentification: ${response.statusCode}");
    print(response.body);
  }
}

// Fonction pour envoyer des détections
Future<void> sendDetections(String accessToken) async {
  if (detections.isEmpty) {
    print("Aucune détection à envoyer.");
    return;
  }

  final headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer $accessToken',
  };

  final response = await http.post(Uri.parse(SEND_DETECTION_URL), headers: headers, body: jsonEncode(detections));

  if (response.statusCode == 200) {
    print("Détections envoyées avec succès.");
  } else if (response.statusCode == 401) {
    print("Token d'accès expiré, il faut le rafraîchir.");
    await refreshTokenFunction();
    await sendDetections(accessToken); // Retente d'envoyer après le rafraîchissement
  } else {
    print("Erreur lors de l'envoi des détections: ${response.statusCode}");
    print(response.body);
  }
}

// Fonction pour rafraîchir le token
Future<void> refreshTokenFunction() async {
  final headers = {'Content-Type': 'application/json'};
  final body = jsonEncode({
    "adressMac": MAC_ADDRESS,
    "refreshToken": refreshToken,
    "withRefreshToken": false,
    "grantType": "refreshToken",
  });

  final response = await http.post(Uri.parse(REFRESH_TOKEN_URL), headers: headers, body: body);

  if (response.statusCode == 200) {
    final data = jsonDecode(response.body);
    accessToken = data['accessToken'];
    validFrom = data['validFrom'];
    validTo = data['validTo'];

    print("Token rafraîchi avec succès.");
    print("Nouveau accessToken: $accessToken");
    print("Licence valide du: $validFrom au: $validTo");
  } else {
    print("Erreur de rafraîchissement: ${response.statusCode}");
    print(response.body);
  }
}

// Fonction pour vérifier si le token est expiré et rafraîchir si nécessaire
Future<String> getValidToken() async {
  DateTime dateToCheck = DateTime.parse(validTo);
  if (DateTime.now().isAfter(dateToCheck)) {
    print("Token expiré, rafraîchissement nécessaire à ${DateTime.now()}");
    await refreshTokenFunction();
  }
  return accessToken;
}




/////
var jscode="";
var currentmedia="default.mp4";
var _controller1;
var controllers={};
var ready=true;
var mlog="";
var senddatas= {};
var maindir='storage/emulated/0/Download';//storage/emulated/0/Download;

var  cameras=[] ;
var streamnotyet=true;
var i=0;
var cc=0;
var mustsend=false;
Future<void> loging() async {
  try {
    // Get the directory where the app can store files

    // Define the path and file name
    String fileName = 'log.txt';
    File file = File('${maindir}/$fileName');

    // Write text to the file
    await file.writeAsString(mlog);

    // Update the UI with the file path and status

  } catch (e) {

  }
}

// Function to get the app's document directory
Future<Directory> _getLocalDirectory() async {
  final directory = await getApplicationDocumentsDirectory();
  return directory;

}
String removeFirstAndLast(String input) {
  // if (input.length <= 2) {
  // If the string has 2 or fewer characters, return an empty string
  // return '';
  //}
  return input.replaceAll("'", "").replaceAll('"', '');
}
Future<void> sendData(Map<String, dynamic> data) async {
  final dio = Dio();
  if(Platform.isAndroid){
    final certificateFile = '''
-----BEGIN CERTIFICATE-----
MIIGNzCCBR+gAwIBAgIQTfKfIOrDHPfgRMdQdhR7TjANBgkqhkiG9w0BAQsFADCB
jzELMAkGA1UEBhMCTUExEjAQBgNVBAgTCU1hcnJha2VjaDESMBAGA1UEBxMJTWFy
cmFrZWNoMR8wHQYDVQQKExZHZW5pb3VzIENvbW11bmljYXRpb25zMTcwNQYDVQQD
Ey5HRU5JT1VTIFJTQSBEb21haW4gVmFsaWRhdGlvbiBTZWN1cmUgU2VydmVyIENB
MB4XDTI0MDgyOTAwMDAwMFoXDTI1MDgyOTIzNTk1OVowEjEQMA4GA1UEAxMHcWlh
cy50djCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOEuifa9vSB9a3XE
QBbSIhPqb8n8QSG6XTQKoHy/e7fqS8mF2VqT6RWZ0KDcmzmCfb8fJKsKr+ww8jZC
kFUIwxLfdC8LcN6tJwI43o7p1IT1VL3RUgnUdifXyMwq0ua2Az+lz7aICx9xuqyl
Mpvtx4sR0bFM3a8obBHRPnsskjdxxybUsixbbP3Dzue/qLKUgpIeOFBdr/Vxksmm
6cssTb/n1stF+GMd+psMOaBdhXJeQ/hy1q1I83yWJWd5YNB5h5G3VtphblyffejE
X4zm+S+jEGSHP0+dyW1MzoI4dmepZiRUlUAAEamWPUtGBj9WTEmU/qBsekDucS35
wXnFkpECAwEAAaOCAwkwggMFMB8GA1UdIwQYMBaAFMYshCxsuVYs20aUIEqrP/9N
lT/PMB0GA1UdDgQWBBRxsQrxU6feTnnVRfTtppg7KMMihTAOBgNVHQ8BAf8EBAMC
BaAwDAYDVR0TAQH/BAIwADAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIw
SQYDVR0gBEIwQDA0BgsrBgEEAbIxAQICQzAlMCMGCCsGAQUFBwIBFhdodHRwczov
L3NlY3RpZ28uY29tL0NQUzAIBgZngQwBAgEwgZYGCCsGAQUFBwEBBIGJMIGGMFcG
CCsGAQUFBzAChktodHRwOi8vZ2VuaW91cy5jcnQuc2VjdGlnby5jb20vR0VOSU9V
U1JTQURvbWFpblZhbGlkYXRpb25TZWN1cmVTZXJ2ZXJDQS5jcnQwKwYIKwYBBQUH
MAGGH2h0dHA6Ly9nZW5pb3VzLm9jc3Auc2VjdGlnby5jb20wHwYDVR0RBBgwFoIH
cWlhcy50doILd3d3LnFpYXMudHYwggF/BgorBgEEAdZ5AgQCBIIBbwSCAWsBaQB2
AN3cyjSV1+EWBeeVMvrHn/g9HFDf2wA6FBJ2Ciysu8gqAAABkZ83DTQAAAQDAEcw
RQIgO4KJdydx1BPF1a3fX9rcGL91B4pWQ+amBKqhd9YqUpECIQCAvQbkfEF1wwZr
yEIwuoP1a6t/d1burAi0hewz0SMWLgB3AA3h8jAr0w3BQGISCepVLvxHdHyx1+kw
7w5CHrR+Tqo0AAABkZ83DL0AAAQDAEgwRgIhAMYaXMAWhnFXs9SByuYxNTGXrAAe
7Uo8BPUaHkjTPPHEAiEAktzZDA+e2ahs2tAlWKJW7mXz7QcsDuuyh2jE+JnQzj0A
dgAS8U40vVNyTIQGGcOPP3oT+Oe1YoeInG0wBYTr5YYmOgAAAZGfNwy9AAAEAwBH
MEUCIQCCtyvRIgyYyl4tVMZXq05Z85F8JrTc3zQ8XkldYzKywgIgOploc654WnJS
sYykxQnibPvgHbhPR3gsqDZ7whoeII0wDQYJKoZIhvcNAQELBQADggEBAJYIxer6
fkzWfUQKlA3t5YXW30mWiza3dH8Y42A5lXyQugP2schTwkoBqdENLYIhv06tyr+h
d03zgus3kizLEP/YVgDns4+M6lgweyCN4AMLv+gRmnFCxlVeV0EKnuNnVhStnloA
Z9ze9AJRk5EkjvOWaaA8fPmGuQQ+JyxFVGVgJAujAqXQEiz5U+YM2H21tcKzHdK7
p6ySf5i87vnXpFxIVmZVBBfeho73e37pJ4FBraYGabm5qs8Qv8elAGaGXwiGmcgb
degHXfnh8tuPTt69gWna9XZaVCaSTBJE7HAIJgr5hA9fBTX5VhUJNcAkbdenurHZ
qq+1IIgPEiK847Y=
-----END CERTIFICATE-----

''';

    final SecurityContext context = SecurityContext.defaultContext;
    context.setTrustedCertificatesBytes(certificateFile.codeUnits);

    final httpClientAdapter = IOHttpClientAdapter(
      createHttpClient: () {
        return HttpClient(context: context);
      },
    );

    dio.httpClientAdapter = httpClientAdapter;
  }
  //final Directory? downloadsDir =  Directory("storage/emulated/0");
  //maindir='storage/emulated/0/Download';
  // if (downloadsDir == null) {
  //  print('Could not get Downloads directory');
  //  return;
  // }

  dio.options.baseUrl = 'https://qias.tv:5000/';  // Replace with your Flask server URL
  dio.options.headers = {
    'Content-Type': 'application/json',  // Specify JSON content type
  };
  // final rules = await dio.get('https://qias.tv:5000/rules/rules');
  final response = await dio.post('/tvdata', data: data);
}
Future<void> downloadVideo() async {
  final dio = Dio();
  if(Platform.isAndroid){
    final certificateFile = '''
-----BEGIN CERTIFICATE-----
MIIGNzCCBR+gAwIBAgIQTfKfIOrDHPfgRMdQdhR7TjANBgkqhkiG9w0BAQsFADCB
jzELMAkGA1UEBhMCTUExEjAQBgNVBAgTCU1hcnJha2VjaDESMBAGA1UEBxMJTWFy
cmFrZWNoMR8wHQYDVQQKExZHZW5pb3VzIENvbW11bmljYXRpb25zMTcwNQYDVQQD
Ey5HRU5JT1VTIFJTQSBEb21haW4gVmFsaWRhdGlvbiBTZWN1cmUgU2VydmVyIENB
MB4XDTI0MDgyOTAwMDAwMFoXDTI1MDgyOTIzNTk1OVowEjEQMA4GA1UEAxMHcWlh
cy50djCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOEuifa9vSB9a3XE
QBbSIhPqb8n8QSG6XTQKoHy/e7fqS8mF2VqT6RWZ0KDcmzmCfb8fJKsKr+ww8jZC
kFUIwxLfdC8LcN6tJwI43o7p1IT1VL3RUgnUdifXyMwq0ua2Az+lz7aICx9xuqyl
Mpvtx4sR0bFM3a8obBHRPnsskjdxxybUsixbbP3Dzue/qLKUgpIeOFBdr/Vxksmm
6cssTb/n1stF+GMd+psMOaBdhXJeQ/hy1q1I83yWJWd5YNB5h5G3VtphblyffejE
X4zm+S+jEGSHP0+dyW1MzoI4dmepZiRUlUAAEamWPUtGBj9WTEmU/qBsekDucS35
wXnFkpECAwEAAaOCAwkwggMFMB8GA1UdIwQYMBaAFMYshCxsuVYs20aUIEqrP/9N
lT/PMB0GA1UdDgQWBBRxsQrxU6feTnnVRfTtppg7KMMihTAOBgNVHQ8BAf8EBAMC
BaAwDAYDVR0TAQH/BAIwADAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIw
SQYDVR0gBEIwQDA0BgsrBgEEAbIxAQICQzAlMCMGCCsGAQUFBwIBFhdodHRwczov
L3NlY3RpZ28uY29tL0NQUzAIBgZngQwBAgEwgZYGCCsGAQUFBwEBBIGJMIGGMFcG
CCsGAQUFBzAChktodHRwOi8vZ2VuaW91cy5jcnQuc2VjdGlnby5jb20vR0VOSU9V
U1JTQURvbWFpblZhbGlkYXRpb25TZWN1cmVTZXJ2ZXJDQS5jcnQwKwYIKwYBBQUH
MAGGH2h0dHA6Ly9nZW5pb3VzLm9jc3Auc2VjdGlnby5jb20wHwYDVR0RBBgwFoIH
cWlhcy50doILd3d3LnFpYXMudHYwggF/BgorBgEEAdZ5AgQCBIIBbwSCAWsBaQB2
AN3cyjSV1+EWBeeVMvrHn/g9HFDf2wA6FBJ2Ciysu8gqAAABkZ83DTQAAAQDAEcw
RQIgO4KJdydx1BPF1a3fX9rcGL91B4pWQ+amBKqhd9YqUpECIQCAvQbkfEF1wwZr
yEIwuoP1a6t/d1burAi0hewz0SMWLgB3AA3h8jAr0w3BQGISCepVLvxHdHyx1+kw
7w5CHrR+Tqo0AAABkZ83DL0AAAQDAEgwRgIhAMYaXMAWhnFXs9SByuYxNTGXrAAe
7Uo8BPUaHkjTPPHEAiEAktzZDA+e2ahs2tAlWKJW7mXz7QcsDuuyh2jE+JnQzj0A
dgAS8U40vVNyTIQGGcOPP3oT+Oe1YoeInG0wBYTr5YYmOgAAAZGfNwy9AAAEAwBH
MEUCIQCCtyvRIgyYyl4tVMZXq05Z85F8JrTc3zQ8XkldYzKywgIgOploc654WnJS
sYykxQnibPvgHbhPR3gsqDZ7whoeII0wDQYJKoZIhvcNAQELBQADggEBAJYIxer6
fkzWfUQKlA3t5YXW30mWiza3dH8Y42A5lXyQugP2schTwkoBqdENLYIhv06tyr+h
d03zgus3kizLEP/YVgDns4+M6lgweyCN4AMLv+gRmnFCxlVeV0EKnuNnVhStnloA
Z9ze9AJRk5EkjvOWaaA8fPmGuQQ+JyxFVGVgJAujAqXQEiz5U+YM2H21tcKzHdK7
p6ySf5i87vnXpFxIVmZVBBfeho73e37pJ4FBraYGabm5qs8Qv8elAGaGXwiGmcgb
degHXfnh8tuPTt69gWna9XZaVCaSTBJE7HAIJgr5hA9fBTX5VhUJNcAkbdenurHZ
qq+1IIgPEiK847Y=
-----END CERTIFICATE-----

''';

    final SecurityContext context = SecurityContext.defaultContext;
    context.setTrustedCertificatesBytes(certificateFile.codeUnits);

    final httpClientAdapter = IOHttpClientAdapter(
      createHttpClient: () {
        return HttpClient(context: context);
      },
    );

    dio.httpClientAdapter = httpClientAdapter;
  }
  //final Directory? downloadsDir =  Directory("storage/emulated/0");
  //maindir='storage/emulated/0/Download';
  // if (downloadsDir == null) {
  //  print('Could not get Downloads directory');
  //  return;
  // }
  try {
    print("start download");
    if(Platform.isIOS){
      print("downloading..model_gender_q.tflite" );
      await dio.download(
        'https://qias.tv:5000/static/model_gender_q.tflite',
        '${maindir}/model_gender_q.tflite',
      );
      print("downloading..model_age_q.tflite" );
      await dio.download(
        'https://qias.tv:5000/static/model_age_q.tflite',
        '${maindir}/model_gender_q.tflite',
      );}
    final rules = await dio.get('https://qias.tv:5000/rules/rules');
    //  Map<String, dynamic> jsonRules = jsonDecode(rules.data.toString());
    //   print(rules.data);
    jscode=rules.data;
    final dirs = await dio.get('https://qias.tv:5000/dirs/dirs');

    //  print(dirs.data.toString().split(","));
    var vid;
    var counter=0;
    var dds=dirs.data.toString().split(",");
    for (String file in dds) {
      counter++;
      print("counter : "+counter.toString());
      //    print(dds);

      print("downloading.." + file);
      await dio.download(
        'https://qias.tv:5000/static/' + file,
        '${maindir}/' + file,
      );


    }
    for (String file in dds) {
      if (file=="ic.js"){}else {

        vid = maindir + file;
        //  vid.setLooping(true);
        //  await vid.initialize();
        //  controllers[file] = vid;
      }}
    ready=true;
    //   print(".....................");

    // print('${downloadsDir.path}/test.mp4');
    //  print('Download completed ');

  } catch (e) {
    //  print('Download failed: $e');
  }
}
//////
var v1={};
Future<void> initializeService() async {
  final service = FlutterBackgroundService();


  /// OPTIONAL, using custom notification channel id
  const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'my_foreground', // id
    'MY FOREGROUND SERVICE', // title
    description:
    'This channel is used for important notifications.', // description
    importance: Importance.max, // importance must be at low or higher level
  );

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();

  if (Platform.isIOS || Platform.isAndroid) {
    await flutterLocalNotificationsPlugin.initialize(
      const InitializationSettings(
        iOS: DarwinInitializationSettings(),
        android: AndroidInitializationSettings('ic_bg_service_small'),
      ),
    );
  }


  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
      AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await service.configure(
    androidConfiguration: AndroidConfiguration(
      // this will be executed when app is in foreground or background in separated isolate
      onStart: onStart,

      // auto start service
      autoStart: true,
      isForegroundMode: true,

      notificationChannelId: 'my_foreground',
      initialNotificationTitle: 'AWESOME SERVICE',
      initialNotificationContent: 'Initializing',
      foregroundServiceNotificationId: 888,
    ),
    iosConfiguration: IosConfiguration(
      // auto start service
      autoStart: true,

      // this will be executed when app is in foreground in separated isolate
      onForeground: onStart,

      // you have to enable background fetch capability on xcode project
      onBackground: onIosBackground,
    ),
  );

  service.startService();
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initializeService();
  Directory directory = await  getApplicationDocumentsDirectory();

  if (Platform.isIOS){maindir=directory.path;}else{maindir=directory.path;};
  await authenticateDevice();
  await downloadVideo();

  // Logger.root.level = Level.SEVERE; // Set the desired log level
  // Logger.root.onRecord.listen((record) {
  // Print logs to the console
  //  print('[${record.level.name}] ${record.time}: ${record.message}');

  // Write logs to a file
  // sendErrorToFlaskerr(record.message);
  // });

  runApp(CameraView());
}
var interpreter;
var croppedFace;
var interpreter2;




@pragma('vm:entry-point')
Future<bool> onIosBackground(ServiceInstance service) async {
  WidgetsFlutterBinding.ensureInitialized();
  DartPluginRegistrant.ensureInitialized();

  SharedPreferences preferences = await SharedPreferences.getInstance();
  await preferences.reload();
  final log = preferences.getStringList('log') ?? <String>[];
  log.add(DateTime.now().toIso8601String());try{
  }catch(e){log.add(e.toString());
  await preferences.setStringList('log', log);}
  return true;
}

@pragma('vm:entry-point')
void onStart(ServiceInstance service) async {
  // Only available for flutter 3.0.0 and later
  DartPluginRegistrant.ensureInitialized();

  // For flutter prior to version 3.0.0
  // We have to register the plugin manually
  SharedPreferences pref1 = await SharedPreferences.getInstance();
  SharedPreferences preferences = await SharedPreferences.getInstance();
  await preferences.setString("hello", "world");

  /// OPTIONAL when use custom notification
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();

  if (service is AndroidServiceInstance) {
    service.on('setAsForeground').listen((event) {
      service.setAsForegroundService();
    });

    service.on('setAsBackground').listen((event) {
      service.setAsBackgroundService();
    });
  }

  service.on('stopService').listen((event) {
    service.stopSelf();
  });

  // bring to foreground
  Timer.periodic(const Duration(seconds: 120), (timer) async { //60
    //   if (service is AndroidServiceInstance) {
    //   if (await service.isForegroundService()) {
    /// OPTIONAL for use custom notification
    /// the notification id must be equals with AndroidConfiguration when you call configure() method.
    //     flutterLocalNotificationsPlugin.show(
    //      888,
    //      'COOL SERVICE',
    //      'Awesome ${DateTime.now()}',
    //      const NotificationDetails(
    //        android: AndroidNotificationDetails(
    //          'my_foreground',
    //          'MY FOREGROUND SERVICE',
    //           icon: 'ic_bg_service_small',
    //           ongoing: true,
    //          ),
    //        ),
    //      );

    // if you don't using custom notification, uncomment this
    // service.setForegroundNotificationInfo(
    //   title: "My App Service",
    //   content: "Updated at ${DateTime.now()}",
    //  );
    //    }
    //   }

    /// you can see this log in logcat
    print('FLUTTER BACKGROUND SERVICE: ${DateTime.now()}');


    //    try{
    //     final url = 'https://qias.pythonanywhere.com/postlist';
    //   //  print(myList);
    //    print("wanna send");
    //    final response = await http.post(
    //      Uri.parse(url),
    //      headers: {
    //        'Content-Type': 'application/json',
    //      },
    //      body: jsonEncode({
    //        'list': json.decode(pref.getString("datas")).values,
    //      }),
    //      );

    //      if (response.statusCode == 200) {
    //        print('List posted successfully!');

    //      } else {
    //      print('Failed to post the list.');
    //      await FlutterRingtonePlayer.playNotification();

    //       print(response.toString()+"");
    //     }
    //    }catch(e){      sendErrorToFlask(e.toString());
    //    }

    print("sendlog 1");


    try{
      //  await FlutterRingtonePlayer.playNotification();
      print("sendlog 2");
      await pref1.reload();
      var datas=pref1.getString("datas");
      print("sendlog data:"+datas.toString());
      String url = "https://showstream-32id.onrender.com/postlist";//"https://qiasvf.pythonanywhere.com/postlist";
      final response = await http.post(Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonEncode({
          'list': json.decode(datas.toString()).values.toList(),//json.decode(pref.getString("datas")).values
        }),);

      if (response.statusCode == 200) {
        print('sendlog List posted successfully!');


        stderr.write("sent");
        await pref1.setString("send", "sent");
        await pref1.reload();
        var responseData = response.body.toString();
        print("-------------"+responseData);}
      else {
        print('sendlog Failed to post the list.');

        print(response.body.toString()+"");
        print("sendlog error:not sent because "+response.body.toString());

      }
      if (datas==null){datas=""; };
      print("sendlog"+pref1.getString("send").toString());
      if(pref1.getString("send")=="send"&&(datas!="")){
        print("sendlog 3");
        stderr.write("got signal and sending");
        print("got signal and sending");
      }}catch(e){print("sendlog error: got error because "+e.toString());};


    // test using external plugin
    final deviceInfo = DeviceInfoPlugin();
    String? device;
    if (Platform.isAndroid) {
      final androidInfo = await deviceInfo.androidInfo;
      device = androidInfo.model;
    }

    if (Platform.isIOS) {
      final iosInfo = await deviceInfo.iosInfo;
      device = iosInfo.model;
    }

//    service.invoke(
    //    'update',
    //  {
    //  "current_date": DateTime.now().toIso8601String(),
    //   "device": device,
    // },
    //  );
  });
}


Future<void> sendErrorToFlaskold(String errorMessage) async {
  print(errorMessage);
  return;
  try {
    final url = Uri.parse('https://qias.pythonanywhere.com/add_error');
    final response = await http.post(url, body: {'error_message': errorMessage.toString()});
    if (response.statusCode == 200) {
      // Error sent successfully
    } else {
      // Handle error cases when sending the error
      print('Failed to send error: ${response.statusCode}');
    }
  } catch (e) {
    // Handle network errors or exceptions when sending the error
    print('Error sending error : $e');
  }
  // print("eroring"+errorMessage);
  stderr.writeln("eroring"+errorMessage);

}

Future<String> getDownloadsPath() async {
  // Get the Downloads directory path
  final directory = await getExternalStorageDirectory();

  if (directory != null) {
    // Return the Downloads folder path
    String downloadsPath = directory.path + "/Download";
    return downloadsPath;
  } else {
    throw Exception("Could not get Downloads directory path");
  }
}
Future<void> sendErrorToFlask(String textToAppend) async {
  return;
  mlog=mlog+"\n"+textToAppend;
  print(textToAppend);
  print(textToAppend);  // Print to console (for debugging)

  // Define log file path
  final logFile = File("storage/emulated/0/Download/logg.txt");

  try {
    // Check if the file exists
    if (await logFile.exists()) {
  // If it exists, append the text
  await logFile.writeAsString(textToAppend + "\n", mode: FileMode.append);
  } else {
  // If the file doesn't exist, create and write to it
  await logFile.writeAsString(textToAppend + "\n", mode: FileMode.write);
  }

  print('Log written to: ${logFile.path}');
  } catch (e) {
  print('Error writing log: $e');
  }

}
Future<void> sendErrorToFlaskerr(String textToAppend) async {
  print(textToAppend);
  return;

}








class ExtendedInputImage {
  final InputImage inputImage;
  final String extraStringData;
  final List<String> extraArrayData;

  ExtendedInputImage({
    required this.inputImage,
    required this.extraStringData,
    required this.extraArrayData,
  });
}
class CameraView extends StatefulWidget {
  CameraView(
      {Key? key})
      : super(key: key);

  /// final Function(InputImage inputImage) onImage;
//  final VoidCallback? onCameraFeedReady;
  // final VoidCallback? onDetectorViewModeChanged;
  // final Function(CameraLensDirection direction)? onCameraLensDirectionChanged;
//  final CameraLensDirection initialCameraLensDirection;

  @override
  State<CameraView> createState() => _CameraViewState();
}

class _CameraViewState extends State<CameraView> {
  static List<CameraDescription> _cameras = [];
  late final Timer timer;
  late final Timer timer1;
  // static const platform = const MethodChannel('google_mlkit_face_detector');
  var dn=DateTime.now();
  var inserts=0;

  var mylist=[];




  late final WebViewController controller;

  var thedevicename;
  FlutterTts flutterTts = FlutterTts();
  // static const platform = const MethodChannel('google_mlkit_face_detector');

  var vars={
    'nombre_senior':0,
    'nombre_Adult':0,
    'nombre_jeune_Adulte':0,
    'nombre_jeune':0,
    'nombre_moyen_age':0,

    'nombre_homme':0,
    'nombre_femme':0,

    'nombre_heureux':0,
    'nombre_content':0,
    'nombre_neutre':0,
    'nombre_mecontent':0,

    'nombre_senior_homme':0,
    'nombre_Adult_homme':0,
    'nombre_jeune_Adulte_homme':0,
    'nombre_jeune_homme':0,

    'nombre_senior_femme':0,
    'nombre_Adult_femme':0,
    'nombre_jeune_Adulte_femme':0,
    'nombre_jeune_femme':0,







    'nombre_senior_heureux':0,
    'nombre_senior_content':0,
    'nombre_senior_neutre':0,
    'nombre_senior_mecontent':0,


    'nombre_Adult_heureux':0,
    'nombre_Adult_content':0,
    'nombre_Adult_neutre':0,
    'nombre_Adult_mecontent':0,

    'nombre_jeune_Adulte_heureux':0,
    'nombre_jeune_Adulte_content':0,
    'nombre_jeune_Adulte_neutre':0,
    'nombre_jeune_Adulte_mecontent':0,

    'nombre_jeune_heureux':0,
    'nombre_jeune_content':0,
    'nombre_jeune_neutre':0,
    'nombre_jeune_mecontent':0,

    'nombre_senior_homme':0,
    'nombre_Adult_homme':0,
    'nombre_jeune_Adulte_homme':0,
    'nombre_jeune_homme':0,

    'nombre_senior_femme':0,
    'nombre_Adult_femme':0,
    'nombre_jeune_Adulte_femme':0,
    'nombre_jeune_femme':0,


    'nombre_heureux_homme':0,
    'nombre_content_homme':0,
    'nombre_neutre_homme':0,
    'nombre_mecontent_homme':0,

    'nombre_heureux_femme':0,
    'nombre_content_femme':0,
    'nombre_neutre_femme':0,
    'nombre_mecontent_femme':0,


    'nombre_moyen_age':0,




    'moyen_senior':0,
    'moyen_Adult':0,
    'moyen_jeune_Adulte':0,
    'moyen_jeune':0,
    'moyen_moyen_age':0,




    'moyen_homme':0,
    'moyen_femme':0,






    'moyen_heureux':0,
    'moyen_content':0,
    'moyen_neutre':0,
    'moyen_mecontent':0,



    'min_concentration':0,
    'moyen_concentration':0,

    'nombre_temps_attention':0,
    'nombre_temps_inattention':0,


    'nombre_ots':0,
    'nombre_attention':0,
    'nombre_inattention':0,
    'moyen_attention':0,
    'moyen_inattention':0,
    "nombre_concentration":0,
    'moyen_sertitude_genre':0,
    'taux_conversion':0,
    'taux_conversion_hommes':0,
    'taux_conversion_femmes':0,
    'taux_conversion_seniors':0,
    'taux_conversion_Adults':0,
    'taux_conversion_jeune_Adultes':0,
    'taux_conversion_jeune':0

  };
  var varsc={
    'cumul_attention':0,
    'cumul_inattention':0,
    'cumul_temps_attention':0,
    'cumul_temps_inattention':0,
    'cumul_ots':0,
    'cumul_temps_ots':0,

    'cumul_senior':0,
    'cumul_Adult':0,
    'cumul_jeune_Adulte':0,
    'cumul_jeune':0,
    'cumul_moyen_age':0,

    'cumul_heureux':0,
    'cumul_content':0,
    'cumul_neutre':0,
    'cumul_mecontent':0,

    'cumul_homme':0,
    'cumul_femme':0

  };
  var varsd;
  final dbHelper = DatabaseHelper.instance;
  void requestCameraPermission() async {
    Directory directory = await  getApplicationDocumentsDirectory();

    if (Platform.isIOS){maindir=directory.path;}else{};
    var status = await Permission.camera.status;
    if (status.isDenied) {
      // You can show a dialog or explanation here to inform the user about why you need the camera permission.
      // Then, request the permission.
      var result = await Permission.camera.request();
      var result1 = await Permission.manageExternalStorage.request();
      if (result.isGranted) {
        // Permission is granted; you can now access the camera.
      } else {
        // Permission is denied; handle it accordingly.
      }
    } else if (status.isGranted) {
      // Permission is already granted; you can access the camera.
    } else if (status.isPermanentlyDenied) {
      // The user has permanently denied camera permission. You may need to guide the user to the app settings to manually enable it.
    }
    status = await Permission.storage.status;
    if (status.isDenied) {
      // You can show a dialog or explanation here to inform the user about why you need the camera permission.
      // Then, request the permission.
      var result = await Permission.storage.request();

      if (result.isGranted) {
        // Permission is granted; you can now access the camera.
      } else {
        // Permission is denied; handle it accordingly.
      }
    } else if (status.isGranted) {
      // Permission is already granted; you can access the camera.
    } else if (status.isPermanentlyDenied) {
      // The user has permanently denied camera permission. You may need to guide the user to the app settings to manually enable it.
    }
  }
  img.Image convertBGRA8888ToImage(CameraImage image) {
    final int width = image.width;
    final int height = image.height;

    // Create a blank image with the dimensions
    var imgLib = img.Image(width, height);

    final Uint8List data = image.planes[0].bytes;
    final int length = width * height;

    for (int i = 0; i < length; i++) {
      final int offset = i * 4;

      // Extract the BGRA values
      final int blue = data[offset];
      final int green = data[offset + 1];
      final int red = data[offset + 2];
      final int alpha = data[offset + 3];

      // Set the pixel values
      imgLib.setPixelRgba(i % width, i ~/ width, red, green, blue, alpha);
    }

    return imgLib;
  }
  _insert() async {
    sendErrorToFlask("starting insertion");
    varsd=varsc;
    var thedevicename=await fetchDeviceId();
    vars['nombre_temps_attention']=DateTime.now().difference(dn).inMilliseconds.round()*vars['nombre_attention']!;
    vars['nombre_temps_inattention']=DateTime.now().difference(dn).inMilliseconds.round()*vars['nombre_inattention']!;
    var devicename= "bahaeandroid";//await fetchDeviceId();
    if(vars['nombre_ots']!>0){


      vars['nombre_senior_mecontent']=vars['nombre_mecontent']!*vars['nombre_senior']!;

      vars['nombre_Adult_mecontent']=vars['nombre_mecontent']!*vars['nombre_Adult']!;

      vars['nombre_jeune_Adulte_mecontent']=vars['nombre_mecontent']!*vars['nombre_jeune_Adulte']!;
      vars['nombre_jeune_mecontent']=vars['nombre_mecontent']!*vars['nombre_jeune']!;


      vars['nombre_senior_neutre']=vars['nombre_neutre']!*vars['nombre_senior']!;

      vars['nombre_Adult_neutre']=vars['nombre_neutre']!*vars['nombre_Adult']!;

      vars['nombre_jeune_Adulte_neutre']=vars['nombre_neutre']!*vars['nombre_jeune_Adulte']!;
      vars['nombre_jeune_neutre']=vars['nombre_neutre']!*vars['nombre_jeune']!;


      vars['nombre_senior_heureux']=vars['nombre_heureux']!*vars['nombre_senior']!;

      vars['nombre_Adult_heureux']=vars['nombre_heureux']!*vars['nombre_Adult']!;

      vars['nombre_jeune_Adulte_heureux']=vars['nombre_heureux']!*vars['nombre_jeune_Adulte']!;
      vars['nombre_jeune_heureux']=vars['nombre_heureux']!*vars['nombre_jeune']!;




      vars['nombre_senior_content']=vars['nombre_content']!*vars['nombre_senior']!;

      vars['nombre_Adult_content']=vars['nombre_content']!*vars['nombre_Adult']!;

      vars['nombre_jeune_Adulte_content']=vars['nombre_content']!*vars['nombre_jeune_Adulte']!;
      vars['nombre_jeune_content']=vars['nombre_content']!*vars['nombre_jeune']!;


      vars['nombre_senior_homme']=vars['nombre_homme']!*vars['nombre_senior']!;

      vars['nombre_Adult_homme']=vars['nombre_homme']!*vars['nombre_Adult']!;

      vars['nombre_jeune_Adulte_homme']=vars['nombre_homme']!*vars['nombre_jeune_Adulte']!;
      vars['nombre_jeune_homme']=vars['nombre_homme']!*vars['nombre_jeune']!;



      vars['nombre_senior_femme']=vars['nombre_femme']!*vars['nombre_senior']!;

      vars['nombre_Adult_femme']=vars['nombre_femme']!*vars['nombre_Adult']!;

      vars['nombre_jeune_Adulte_femme']=vars['nombre_femme']!*vars['nombre_jeune_Adulte']!;
      vars['nombre_jeune_femme']=vars['nombre_femme']!*vars['nombre_jeune']!;


      vars['nombre_heureux_homme']=vars['nombre_homme']!*vars['nombre_heureux']!;

      vars['nombre_content_homme']=vars['nombre_homme']!*vars['nombre_content']!;

      vars['nombre_neutre_homme']=vars['nombre_homme']!*vars['nombre_neutre']!;
      vars['nombre_mecontent_homme']=vars['nombre_homme']!*vars['nombre_mecontent']!;


      vars['nombre_heureux_femme']=vars['nombre_femme']!*vars['nombre_heureux']!;

      vars['nombre_content_femme']=vars['nombre_femme']!*vars['nombre_content']!;

      vars['nombre_neutre_femme']=vars['nombre_femme']!*vars['nombre_neutre']!;
      vars['nombre_mecontent_femme']=vars['nombre_femme']!*vars['nombre_mecontent']!;





      vars['taux_conversion']=(((vars['nombre_attention']!/vars['nombre_ots']!))*100).round();

      vars['taux_conversion_hommes']=(((vars['nombre_homme']!/vars['nombre_ots']!))*100).round();
      vars['taux_conversion_femmes']=(((vars['nombre_femme']!/vars['nombre_ots']!))*100).round();
      vars['taux_conversion_seniors']=(((vars['nombre_senior']!/vars['nombre_ots']!))*100).round();
      vars['taux_conversion_Adults']=(((vars['nombre_Adult']!/vars['nombre_ots']!))*100).round();
      vars['taux_conversion_jeune_Adultes']=(((vars['nombre_jeune_Adulte']!/vars['nombre_ots']!))*100).round();
      vars['taux_conversion_jeune']=(((vars['nombre_jeune']!/vars['nombre_ots']!))*100).round();

      vars['moyen_attention']=(((vars['nombre_attention']!/vars['nombre_ots']!))*100).round();
      vars['moyen_inattention']=(((vars['nombre_inattention']!/vars['nombre_ots']!))*100).round();
      vars['moyen_senior']=(((vars['nombre_senior']!/vars['nombre_ots']!))*100).round();
      vars['moyen_Adult']=(((vars['nombre_Adult']!/vars['nombre_ots']!))*100).round();
      vars['moyen_jeune_Adulte']=(((vars['nombre_jeune_Adulte']!/vars['nombre_ots']!))*100).round();
      vars['moyen_jeune']=(((vars['nombre_jeune']!/vars['nombre_ots']!))*100).round();
      vars['moyen_homme']=(((vars['nombre_homme']!/vars['nombre_ots']!))*100).round();
      vars['moyen_femme']=(((vars['nombre_femme']!/vars['nombre_ots']!))*100).round();

      vars['moyen_heureux']=(((vars['nombre_heureux']!/vars['nombre_ots']!))*100).round();
      vars['moyen_content']=(((vars['nombre_content']!/vars['nombre_ots']!))*100).round();
      vars['moyen_neutre']=(((vars['nombre_neutre']!/vars['nombre_ots']!))*100).round();
      vars['moyen_mecontent']=(((vars['nombre_mecontent']!/vars['nombre_ots']!))*100).round();

      varsd['cumul_attention']=vars['nombre_attention']!+varsd['cumul_attention']!;
      varsd['cumul_inattention']=vars['nombre_inattention']!+varsd['cumul_inattention']!;
      varsd['cumul_senior']=vars['nombre_senior']!+varsd['cumul_senior']!;
      varsd['cumul_Adult']=vars['nombre_Adult']!+varsd['cumul_Adult']!;
      varsd['cumul_jeune_Adulte']=vars['nombre_jeune_Adulte']!+varsd['cumul_jeune_Adulte']!;
      varsd['cumul_jeune']=vars['nombre_jeune']!+varsd['cumul_jeune']!;
      varsd['cumul_homme']=vars['nombre_homme']!+varsd['cumul_homme']!;
      varsd['cumul_femme']=vars['nombre_femme']!+varsd['cumul_femme']!;
      varsd['cumul_heureux']=vars['nombre_heureux']!+varsd['cumul_heureux']!;
      varsd['cumul_content']=vars['nombre_content']!+varsd['cumul_content']!;
      varsd['cumul_neutre']=vars['nombre_neutre']!+varsd['cumul_neutre']!;
      varsd['cumul_mecontent']=vars['nombre_mecontent']!+varsd['cumul_mecontent']!;
      varsd['cumul_neutre']=vars['nombre_neutre']!+varsd['cumul_neutre']!;
      varsd['cumul_moyen_age']=vars['moyen_moyen_age']!+varsd['cumul_moyen_age']!;

      varsd['cumul_temps_attention']=vars['nombre_temps_attention']!+varsd['cumul_temps_attention']!;
      varsd['cumul_temps_inattention']=vars['nombre_temps_inattention']!+varsd['cumul_temps_inattention']!;
      varsd['cumul_moyen_age']=((vars['moyen_moyen_age']!+varsd['cumul_moyen_age']!)/2).ceil();
    };
    //varsd.addAll(varsc)

    Map<String, dynamic> row = {


      DatabaseHelper.column_nombre_senior : vars['nombre_senior'],
      DatabaseHelper.column_nombre_Adult : vars['nombre_Adult'],
      DatabaseHelper.column_nombre_jeune_Adulte : vars['nombre_jeune_Adulte'],
      DatabaseHelper.column_nombre_jeune : vars['nombre_jeune'],
      DatabaseHelper.column_nombre_moyen_age : vars['nombre_moyen_age'],



      DatabaseHelper.column_moyen_senior : vars['moyen_senior'],
      DatabaseHelper.column_moyen_Adult : vars['moyen_Adult'],
      DatabaseHelper.column_moyen_jeune_Adulte : vars['moyen_jeune_Adulte'],
      DatabaseHelper.column_moyen_jeune : vars['moyen_jeune'],
      DatabaseHelper.column_moyen_moyen_age : vars['moyen_moyen_age'],


      DatabaseHelper.column_nombre_homme : vars['nombre_homme'],
      DatabaseHelper.column_nombre_femme : vars['nombre_femme'],

      DatabaseHelper.column_moyen_homme : vars['moyen_homme'],
      DatabaseHelper.column_moyen_femme : vars['moyen_femme'],




      DatabaseHelper.column_nombre_heureux : vars['nombre_heureux'],
      DatabaseHelper.column_nombre_content : vars['nombre_content'],
      DatabaseHelper.column_nombre_neutre : vars['nombre_neutre'],
      DatabaseHelper.column_nombre_mecontent : vars['nombre_mecontent'],

      DatabaseHelper.column_moyen_heureux : vars['moyen_heureux'],
      DatabaseHelper.column_moyen_content : vars['moyen_content'],
      DatabaseHelper.column_moyen_neutre : vars['moyen_neutre'],
      DatabaseHelper.column_moyen_mecontent : vars['moyen_mecontent'],



      DatabaseHelper.column_min_concentration : vars['min_concentration'],
      DatabaseHelper.column_moyen_concentration : vars['moyen_concentration'],

      DatabaseHelper.column_nombre_temps_attention : vars['nombre_temps_attention'],
      DatabaseHelper.column_nombre_temps_inattention : vars['nombre_temps_inattention'],


      DatabaseHelper.column_nombre_ots : vars['nombre_ots'],
      DatabaseHelper.column_nombre_attention : vars['nombre_attention'],
      DatabaseHelper.column_nombre_inattention : vars['nombre_inattention'],
      DatabaseHelper.column_moyen_attention : vars['moyen_attention'],
      DatabaseHelper.column_moyen_inattention : vars['moyen_inattention'],



      DatabaseHelper.column_cumul_attention : varsd['cumul_attention'],
      DatabaseHelper.column_cumul_inattention : varsd['cumul_inattention'],
      DatabaseHelper.column_cumul_temps_attention : varsd['cumul_temps_attention'],
      DatabaseHelper.column_cumul_temps_inattention : varsd['cumul_temps_inattention'],
      DatabaseHelper.column_cumul_ots : varsd['cumul_ots'],
      DatabaseHelper.column_cumul_temps_ots : varsd['cumul_temps_ots'],

      DatabaseHelper.column_cumul_senior : varsd['cumul_senior'],
      DatabaseHelper.column_cumul_Adult : varsd['cumul_Adult'],
      DatabaseHelper.column_cumul_jeune_Adulte : varsd['cumul_jeune_Adulte'],
      DatabaseHelper.column_cumul_jeune : varsd['cumul_jeune'],
      DatabaseHelper.column_cumul_moyen_age : varsd['cumul_moyen_age'],

      DatabaseHelper.column_cumul_heureux : varsd['cumul_heureux'],
      DatabaseHelper.column_cumul_content : varsd['cumul_content'],
      DatabaseHelper.column_cumul_neutre : varsd['cumul_neutre'],
      DatabaseHelper.column_cumul_mecontent : varsd['cumul_mecontent'],

      DatabaseHelper.column_cumul_homme : varsd['cumul_homme'],
      DatabaseHelper.column_cumul_femme : varsd['cumul_femme'],



      DatabaseHelper.columnNomAppareil:devicename,
      DatabaseHelper.columnDate:dn.year.round().toString()+"-"+dn.month.round().toString()+"-"+dn.day.round().toString(),
      DatabaseHelper.columntime:dn.hour.round().toString()+":"+dn.minute.round().toString()+":"+dn.second.round().toString(),
      DatabaseHelper.columnDatetime:dn.toIso8601String()
    };
    // Change the text dynamically here.
        ;

    sendErrorToFlask("almost starting insertion");
    await pref.reload();
//print(row);
    if(vars['nombre_ots'].toString()!='0'){
      v1.clear();v1.addAll(vars);

      Map<String, int> mergedMap = {...v1, ...varsd};
      //   mylist.add( mergedMap);
      sendErrorToFlask("before starting insertion");
      //   if(pref.getString("send")=="sent"){
      // senddatas.clear();
      //   await pref.setString("send", "ready to send again");
      //  await pref.reload();

      //}
      //senddatas[senddatas.length.toString()]={...vars,"devicename":thedevicename.toString(),"date":dn.year.round().toString()+"-"+dn.month.round().toString()+"-"+dn.day.round().toString(),"time":dn.hour.round().toString()+":"+dn.minute.round().toString()+":"+dn.second.round().toString(),"datetime":dn.toIso8601String()};

      // print(senddatas.length);
      //   print("state"+pref.getString("send"));



    }else{
      sendErrorToFlask("no detection");
    };

    //print('inserted row id: $inserts');
    // inserts++;
    //   if (inserts%2==0){inserts=0;print(senddatas.last);   sendErrorToFlask("sending");
    //  }

        ;
  }
  _commit() async {

    final id = await dbHelper.commit();
    sendErrorToFlask("committing");

    // print('inserted row id: $id');
  }
  _query() async {
    List<Map<String, dynamic>> allRows = await dbHelper.queryAllRowsd();
    print(allRows.last);
    //  postListToServer(allRows);
    sendErrorToFlask("data sended succesfully");

    //  print('query all rows:');
    // print(allRows.length);
    //   print('querried');

    // allRows.forEach((row) => print(row));
  }
  late SharedPreferences pref ;
  var cc=0;
  CameraController? _controller;
  var interpreter;
  var interpreter2;
  var _cameraLensDirection = CameraLensDirection.front;
  var formattedImage;
  var inputImage;

  var speaking=false;
  int _cameraIndex = -1;
  double _currentZoomLevel = 1.0;
  double _minAvailableZoom = 1.0;
  double _maxAvailableZoom = 1.0;
  double _minAvailableExposureOffset = 0.0;
  double _maxAvailableExposureOffset = 0.0;
  double _currentExposureOffset = 0.0;
  bool _changingCameraLens = false;
  String dynamicText = 'Google ML Kit Demo App';
  final TextEditingController _controllertext = TextEditingController();


  var _faceDetector = FaceDetector(
    options: FaceDetectorOptions(
      enableContours: true,
      enableLandmarks: true,
      enableClassification: true,
      minFaceSize: 0.3,

    ),
  );
  Future<String?> fetchDeviceId() async {
    final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    try {
      AndroidDeviceInfo androidInfo = await deviceInfoPlugin.androidInfo;
      return androidInfo.id;  // Returns the Android ID
    } catch (androidError) {
      try {
        IosDeviceInfo iosInfo = await deviceInfoPlugin.iosInfo;
        return iosInfo.identifierForVendor;  // Returns the Identifier for Vendor (iOS)
      } catch (iosError) {
        print("Failed to get device ID: $iosError");
      }
      print("Failed to get device ID: $androidError");
    }
    return "inconnu";
  }

  @override
  void initState()  {
    super.initState();

    var ff=File(maindir+"/default.mp4");
    _controller1 = VideoPlayerController.file(ff);
    _controller1..setLooping(true)
      ..initialize().then((_) {
        setState(() {
          _controller1.play();
        });
      }).catchError((error) {
        //   print('Error initializing video player: $error');
      });


    try{
      requestCameraPermission();    sendErrorToFlask("permition granted");
    }catch(e){print(e);};
    controller = WebViewController(onPermissionRequest: handlePermissionRequest)
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
    //    ..addJavaScriptChannel('Flutter', onMessageReceived: (JavaScriptMessage message) {
    //    _processImage(message.message);
    //  })
      ..setBackgroundColor(const Color(0x00000000))
      ..setOnConsoleMessage((message) {
        //  print(message);
      })

      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            // Update loading bar.
          },
          onPageStarted: (String url) {},
          onPageFinished: (String url) {},

          onWebResourceError: (WebResourceError error) {},
          onNavigationRequest: (NavigationRequest request) {
            if (request.url.startsWith('https://www.youtube.com/')) {
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
        ),
      );


    if (controller.platform is AndroidWebViewController) {
      AndroidWebViewController.enableDebugging(true);
      (controller.platform as AndroidWebViewController)
          .setMediaPlaybackRequiresUserGesture(false);
    }

    Timer.periodic(const Duration(seconds: 20), (timer1) async {
try{
  var ac=await getValidToken();
  await sendDetections(ac);detections.clear();}catch(e){sendErrorToFlask(e.toString());};print("---================");print(detections);});
    Timer(const Duration(seconds: 3), () async {

      // _cameras= await availableCameras();;
      var status = await Permission.phone.status;

      await startServer();
      controller.loadRequest(Uri.parse("http://localhost:8080"));
      print("starting");

      if (Platform.isIOS){Directory directory = await getApplicationDocumentsDirectory();maindir=directory.path;}else{};
      //  print(directory.path);

      if (status.isGranted){

        print("found cameras");
        await  _startLiveFeed();
      }else{

        _faceDetector = FaceDetector(
          options: FaceDetectorOptions(
            enableContours: true,
            enableLandmarks: true,
            enableClassification: true,
            minFaceSize: 0.3,

          ),
        );


        try{
          var res = await _inputImageFromCameraImagetv(controller);}
        catch(e){sendErrorToFlask(e.toString());};
        //  await _speak("started detection");
        //  setState(() {

        //   showImagePopup(context);

        //  print("new");

      }

    });

    sendErrorToFlask("selecting camera");

    WidgetsFlutterBinding.ensureInitialized();



    var i=1;

    _cameraIndex = i;
    sendErrorToFlask("camera front selected");

    // List<Map<String, dynamic>> allRows = await dbHelper.queryAllRows();
    //  varsd=allRows.last;
  }
  Future<void> _speak(String text) async {
    // Wait for the previous speech to finish
    if (speaking) {
      return;
    }
    if (text=="") {
      return;
    }
    setState(() {
      speaking = true;
    });
    await flutterTts.speak(text);
  }


  @override
  void dispose() {
    //  _stopLiveFeed();
    super.dispose();
  }
  Uint8List convertImageToNV21tv(Uint8List imageData) {
    int width = 1280;
    int height = 720;

    final image = img.decodeImage(imageData);

    if (image == null) {
      throw ArgumentError('Unable to decode image.');
    }

    final yuv = Uint8List(width * height * 3 ~/ 2);

    int yIndex = 0;
    int uvIndex = width * height;
    final imgPixels = image.getBytes();

    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        final pixelIndex = (i * width + j) * 4;
        final r = imgPixels[pixelIndex + 0];
        final g = imgPixels[pixelIndex + 1];
        final b = imgPixels[pixelIndex + 2];

        // Calculate Y, U, V values
        final y = (0.299 * r + 0.587 * g + 0.114 * b).round();
        final u = ((b - y) * 0.493).round() + 128;
        final v = ((r - y) * 0.877).round() + 128;

        // Write Y component
        yuv[yIndex++] = y;

        // Write UV components (4:2:0 subsampling)
        if ((i % 2 == 0) && (j % 2 == 0)) {
          yuv[uvIndex++] = u;
          yuv[uvIndex++] = v;
        }
      }
    }


    return yuv;
  }
  Future<InputImage?> _inputImageFromCameraImagetv(controller)async {
    const cut=0;
    var at=0;
    var it=0;
    var aud=false;

    // get image rotation
    // it is used in android to convert the InputImage from Dart to Java: https://github.com/flutter-ml/google_ml_kit_flutter/blob/master/packages/google_mlkit_commons/android/src/main/java/com/google_mlkit_commons/InputImageConverter.java
    // `rotation` is not used in iOS to convert the InputImage from Dart to Obj-C: https://github.com/flutter-ml/google_ml_kit_flutter/blob/master/packages/google_mlkit_commons/ios/Classes/MLKVisionImage%2BFlutterPlugin.m
    // in both platforms `rotation` and `camera.lensDirection` can be used to compensate `x` and `y` coordinates on a canvas: https://github.com/flutter-ml/google_ml_kit_flutter/blob/master/packages/example/lib/vision_detector_views/painters/coordinates_translator.dart






    // compose InputImage using bytes
    Future<void> _process(controller) async {

      //  if(mylist.length>10){

      //  }

      //  print("+++++++");
      // print(faces.length);

      //print("+++++++");


      final imageUrl = await controller.runJavaScriptReturningResult('document.getElementById("canvas").toDataURL("image/png");');

      //   print(imageUrl);
      final Uint8List bytess= base64Decode(imageUrl.toString().replaceAll('"', '').split('data:image/png;base64,').last);
      Image image = Image.memory(bytess);
      final imagee = img.decodeImage(bytess)!.getBytes();
      formattedImage=convertImageToNV21tv(bytess);
      final inputImage = InputImage.fromBytes(
        bytes: formattedImage,
        metadata: InputImageMetadata(
            size: me.Size(1280.toDouble(), 720.toDouble()),
            rotation: InputImageRotation.rotation0deg, // used only in Android
            format: InputImageFormat.nv21, // used only in iOS
            bytesPerRow: 1280 // used only in iOS
        ),
      );
      double calculateDistanceRatio(double ax, double ay, double bx, double by, double cx, double cy) {
        // Calculate distance between A and B
        double distance1 = sqrt(pow(bx - ax, 2) + pow(by - ay, 2));

        // Calculate distance between C and B
        double distance2 = sqrt(pow(bx - cx, 2) + pow(by - cy, 2));

        if (distance2 == 0) {
          return 0;// throw Exception("Distance between C and B is zero, ratio is undefined.");
        }

        // Calculate ratio and convert to percentage
        var rs=distance1 / distance2;
        if (rs>1){rs=rs-1;}
        return (rs) * 100;
      }
      String detectMood({required double smileProb}) {

        if (smileProb > 0.86) {

          vars['nombre_heureux']=vars['nombre_heureux']!+1;

          return 'HAPPY';

        }
        else if (smileProb > 0.8) {
          vars['nombre_content']=vars['nombre_content']!+1;
          return 'SATISFIED';

        }

        else if (smileProb > 0.3) {
          vars['nombre_neutre']=vars['nombre_neutre']!+1;

          return 'NEUTRAL';

        }

        else {
          vars['nombre_mecontent']=vars['nombre_mecontent']!+1;

          return 'DISSATISFIED';

        }

      }
      String detectAge({required int ageProb}) {
        if (ageProb > 50) {
          vars['nombre_senior']=vars['nombre_senior']!+1;
          return 'SENIOR';
        }

        else if (ageProb > 35) {
          vars['nombre_Adult']=vars['nombre_Adult']!+1;

          return 'ADULT';
        }

        else if (ageProb > 18) {
          vars['nombre_jeune_Adult']=vars['nombre_jeune_Adult']!+1;
          return 'YOUNG_ADULT';
        }

        else {
          vars['nombre_jeune']=vars['nombre_jeune']!+1;
          return 'YOUTH';
        }
      }
      String detectGender({required List genderProb}) {
        print("genderProb");
        print(genderProb);
        if (genderProb[0].last < genderProb[0].first) {
          vars['nombre_homme']=vars['nombre_homme']!+1;
          //   print("homme+++++++++++++++");
          _controllertext.text="1-"+genderProb.toString();
          return 'MALE';
        }

        else {
          vars['nombre_femme']=vars['nombre_femme']!+1;
          //   print("femme---------------");
          _controllertext.text="2-"+genderProb.toString();
          return 'FEMALE';
        }
      }
      var mp="";

      final faces = await _faceDetector.processImage(inputImage);
      // print(faces);
      vars['nombre_ots']=faces.length;
      //  sendErrorToFlask("face detected"+faces.length.toString());
      //   print("++++++++++++++++++++++++++++++++++"+faces.length.toString());
      var ml=2;
      var per=0;
      //if (faces.length>0){  mp=" ";    };
      vars['nombre_distance']=0;
     const cut=0;
      for (final face in faces) {
        per=per+1;
        sendErrorToFlask("loping over faces");
        final rect = face.boundingBox;


        if(rect.width.toInt()>vars['nombre_distance']!){
          vars['nombre_distance']=((rect.height.toInt()/720)*100).ceil();;
          print("distance :"+vars['nombre_distance'].toString()+"%");
        };
//convertYUV420ToImage(formattedImage)
        try{
          sendErrorToFlask("croping  for gender age detection methode 1");
          croppedFace = img.copyCrop(
            img.Image.fromBytes(1280,720 , formattedImage),
            rect.left.toInt(),
            rect.top.toInt(),
            rect.width.toInt(),
            rect.height.toInt(),
          );}catch(e){
          sendErrorToFlask("croping  for gender age detection methode 2");
          croppedFace = img.copyCrop(
            img.Image.fromBytes(1280,720 , imagee),
            rect.left.toInt(),
            rect.top.toInt()-(rect.width*0.2).toInt(),
            rect.width.toInt(),
            rect.height.toInt()+(rect.width*0.2).toInt(),
          );
        }
        List<int> pngBytes = img.encodePng(croppedFace);
      //  final file = File("storage/emulated/0/Download/croped_${DateTime.now().millisecondsSinceEpoch}.png");
    //    await file.writeAsBytes(pngBytes);
        //  print(croppedFace);
        // print(rect.height.toInt());
        sendErrorToFlask("image croped to fit model conditions of first model for age");

        //   TensorImage tensorImage = TensorImage.fromImage(croppedFace);
        //  final ImageProcessor imageProcessor = ImageProcessorBuilder()
        //    .add(ResizeOp(200, 200,
        //  ResizeMethod.NEAREST_NEIGHBOUR)) // Assuming model expects 224x224
        //  .build();
        sendErrorToFlask("tensorflow image created");

        // img.Image imgg = img.decodeImage(tensorImage.buffer as List<int>)!;
        img.Image resizedImg = img.copyResize(
            croppedFace, width: 200, height: 200);
        sendErrorToFlask("image croped to fit second model for gender detection");

        List<double> tensorData = List.filled(200 * 200 * 3, 0.0);
        for (int y = 0; y < resizedImg.height; y++) {
          for (int x = 0; x < resizedImg.width; x++) {
            int pixel = resizedImg.getPixel(x, y);
            tensorData[(y * resizedImg.width + x) * 3 + 0] =
                img.getRed(pixel) / 255.0;
            tensorData[(y * resizedImg.width + x) * 3 + 1] =
                img.getGreen(pixel) / 255.0;
            tensorData[(y * resizedImg.width + x) * 3 + 2] =
                img.getBlue(pixel) / 255.0;
          }
        }
        sendErrorToFlask("image converted to fit first model");


        var input = tensorData.reshape([1, 200, 200, 3]);
        ; //List.filled(224 * 224 * 3, 0.0).reshape([1, 224, 224, 3]);
        // print(input);
        //  var input=tensorImage.buffer;
        // print(input);
        // Replace with your model's expected input shape and dummy data
        var output = List.generate(1, (i) => List.filled(1, 0.0)); // Replace with your model's expected output shape and dummy data
        //   var output2 = List.generate(1, (i) => List.filled(1, 0.0)); // Replace with your model's expected output shape and dummy data
        var output2 = List.generate(1, (i) => List<double>.filled(2, 0.0));

        //print(output);
        sendErrorToFlask("running first detection");
        interpreter?.run(input, output);
        if(output[0][0]==[[0.0]]){return;}
        sendErrorToFlask("result of age detection "+output.toString());
        if (Platform.isIOS ) {
          //interpreter?.run(input, output);
        } else if (Platform.isAndroid) {

          try {
            List<double> doubleList = [];

            for (var item in input) {
              if (item is num) {
                doubleList.add(item.toDouble());
              } else {
                // Handle cases where the element is not a number (e.g., skip or handle errors)
                // You can also choose to throw an exception if needed.
              }
            }
            //       var processedImageBuffer = imageProcessor
            //         .process(tensorImage)
            //       .buffer;
            //   output = await platform.invokeMethod(
            //       'runInference', {'input': doubleList});
            sendErrorToFlask("first detection finished");
          }catch(e){sendErrorToFlask(e.toString());};

        };

        sendErrorToFlask("croping for second model ");

        //    final ImageProcessor imageProcessor2 = ImageProcessorBuilder()
        //      .add(ResizeOp(200, 200,
        //    ResizeMethod.NEAREST_NEIGHBOUR)) // Assuming model expects 224x224
        //  .build();
        // img.Image imgg = img.decodeImage(tensorImage.buffer as List<int>)!;
        img.Image resizedImg2 = img.copyResize(
            croppedFace, width: 128, height: 128);
        List<double> tensorData2 = List.filled(128 * 128 * 3, 0.0);

        for (int y = 0; y < resizedImg2.height; y++) {
          for (int x = 0; x < resizedImg2.width; x++) {
            int pixel = resizedImg2.getPixel(x, y);
            tensorData2[(y * resizedImg2.width + x) * 3 + 0] =
                img.getRed(pixel) / 255.0;
            tensorData2[(y * resizedImg2.width + x) * 3 + 1] =
                img.getGreen(pixel) / 255.0;
            tensorData2[(y * resizedImg2.width + x) * 3 + 2] =
                img.getBlue(pixel) / 255.0;
          }
        }
        sendErrorToFlask("image converted to fit the second model");

        var input2 = tensorData2.reshape([1, 128, 128, 3]);
        ; //List.filled(224 * 224 * 3, 0.0).reshape([1, 224, 224, 3]);
        sendErrorToFlask("starting the second detection");
        var myage=0;
        //  var mygender = detectGender(genderProb: output2[0][0]);
        // myage=(output[0][0] * 116).ceil();



        // var mygender = detectGender(genderProb: face.leftEyeOpenProbability!);
        //myage=face.rightEyeOpenProbability!.ceil();
        // var processedImageBuffer2 = imageProcessor2.process(tensorImage).buffer;
        // output2 = await platform.invokeMethod('runInference2', {'input': doubleList2});
        interpreter2?.run(input2, output2);
        sendErrorToFlask("result of age detection "+output2.toString());
        var mygender = detectGender(genderProb: output2);
        myage=(output[0][0] * 116).ceil();
        vars['moyen_sertitude_genre']=((((output2[0][0]*100).ceil()+vars['moyen_sertitude_genre']!)/2)*ml).ceil();
        sendErrorToFlask("the age is"+myage.toString());


        sendErrorToFlask("finished detection");

        //  var mygender = detectGender(genderProb: output2[0][0]);
        sendErrorToFlask("gender detection classified");


        //output[0][0] * 116;
        sendErrorToFlask("age detection calculated");

        var mytrage = detectAge(ageProb: myage);
        sendErrorToFlask("age detection clasiffied");
        var myemotion="";
        try{ myemotion = detectMood(smileProb:face.smilingProbability!);}catch(e){};

        sendErrorToFlask("smile detection classified");

        var landmarksDict = face.landmarks.map((key, value) {
          return MapEntry(key.name, value);
        });
        sendErrorToFlask("gathering informations for concentration calculation ");

        // var myfocus=0;


        var ax = landmarksDict["leftMouth"]!.position.x.toDouble();
        var ay = landmarksDict["leftMouth"]!.position.y.toDouble();
        var bx = landmarksDict["noseBase"]!.position.x.toDouble();
        var by = landmarksDict["noseBase"]!.position.y.toDouble();
        var cx = landmarksDict["rightMouth"]!.position.x.toDouble();
        var cy = landmarksDict["rightMouth"]!.position.y.toDouble();
        sendErrorToFlask("informations for concentration calculation gotten ");

        var myfocus = calculateDistanceRatio(ax, ay, bx, by, cx, cy).round();
        sendErrorToFlask(" concentration calculated ");
        vars['nombre_concentration']=myfocus.abs();
        //   print(myfocus);
        if(vars['min_concentration']==0){vars['min_concentration']=myfocus;}else if(vars['min_concentration']!<myfocus){vars['min_concentration']=myfocus;};
        //  sendErrorToFlask("gmin concentration ");


        if(myfocus.abs()>65){vars['nombre_attention']=vars['nombre_attention']!+1;}else{vars['nombre_inattention']=vars['nombre_inattention']!+1;}
        vars['moyen_concentration']=(((myfocus.abs()+vars['moyen_concentration']!)/2)*ml).ceil();
        vars['moyen_moyen_age']=(((myage+vars['moyen_moyen_age']!)/2)*ml).ceil();
        var direction;
        if(myfocus>0){direction="<< left";} else{direction="right >>";};
        // print(mygender);

        //   print(direction);
        //
        //   await _insert();

        var e=0;
        e=vars['nombre_concentration']!;
        ml=1;
        //    if (mygender=='Homme'){mp=mp+"un";}else{mp=mp+"une";}
        //    mp=mp+" "+mytrage+" ";
        //  mp=mp+myemotion;
        mp=mp+" concentration "+e.toString() ;
        mp=mp+"cumul concentration "+vars["cumul_attention"].toString() ;
        //  print("-?-?-?------");
        //  if (e>49){mp=mp+" concentrer";mp=mp+" temps concentration "+vars["cumul_attention"]!.toString();}else{mp=mp+" non concentrer";};
sendErrorToFlask(mp);
//print("showed");
        mp=mp+" et ";
        var now = DateTime.now();

        var formatter = DateFormat("yyyy-MM-ddTHH:mm:ss");
        String formattedDate = formatter.format(now);

         at=0;
         it=0;
         aud=false;
        if (per==1){
          ms.add(now.millisecondsSinceEpoch);
        if (ms.length>1){
          while (ms.length > 2) {
            ms.removeAt(0); // Removes the first element
          }
          var now = DateTime.now();
          // Calculate the difference between the two DateTime objects

          print(cut);
          print("----");
          aud=myfocus.abs()>60;
          if(aud){

            var cd={
              'gender':mygender,
              'emotionalState':myemotion,
              'ageGroup':mytrage,
              'distance':((rect.height.toInt()/720)*100).ceil(),
              'attentionTime':(ms[ms.length-1]-ms[ms.length-2]),
              'innatentionTime':0,
              'audience':aud,
              'detectionDateTime':formattedDate

            };
            print(cd);
            detections.add(cd);
          }else{

      var cd={
      'gender':mygender,
      'emotionalState':myemotion,
      'ageGroup':mytrage,
      'distance':((rect.height.toInt()/720)*100).ceil(),
      'attentionTime':0,
      'innatentionTime':(ms[ms.length-1]-ms[ms.length-2]),
      'audience':aud,
      'detectionDateTime':formattedDate

      };
      print(cd);
      detections.add(cd);

          }
        };}



      };

      mp=mp+".";

      mp=mp.replaceAll('et .','.');//.replaceAll('mecontent','malheureux').replaceAll('content','heureux');
      //   if(0<vars['nombre_distance']! && vars['nombre_distance']!<30){_speak("Veuillez vous approcher de l’écran");
      //   vars['nombre_distance']=0;
      // };

print(mp);
print("detection");
      // _speak(mp);
      //  await _insert();
      // controller.runJavaScriptReturningResult('document.getElementById("canvas").toDataURL("image/png");');
      //controller.runJavaScript('document.getElementById("error-container").innerHTML = "'+vars.toString()+'"');
      //  print(jscode);//, ...varsd

      var datetimeString = "${dn.year}-${dn.month.toString().padLeft(2, '0')}-${dn.day.toString().padLeft(2, '0')} "
          "${dn.hour.toString().padLeft(2, '0')}:${dn.minute.toString().padLeft(2, '0')}:${dn.second.toString().padLeft(2, '0')}";
      var nv={
        ...vars,
        "devicename": "tesdevice",
        "date": "${dn.year}-${dn.month}-${dn.day}",
        "time": "${dn.hour}:${dn.minute}:${dn.second}",
        "datetime": datetimeString,
      };
      var jsonString = jsonEncode(nv);

      try {
        if(per>0){ await sendData(nv);}



      }catch(e){
        print(e.toString());
      }
      print(jsonString);
      var txt = "$jscode; inds=$jsonString; getVideo()";
      //   var rs="";
      var rs = await controller.runJavaScriptReturningResult(txt);
      var answer=rs.toString();
      //  print(vars["nombre_homme"]);
      //   print(answer);
      if (ready){
        if (currentmedia!=answer) {
          setState(() {
            currentmedia=answer;
            // controllers[answer]
            // Stop the current video
            _controller1.pause();
            // Change to the next video URL

            // Dispose of the current controller
            _controller1.dispose();
            // Create a new controller for the new video

            var fi='${maindir}/' +removeFirstAndLast(answer);

            //  print(fi);
            var ff=File(fi);
            _controller1 = VideoPlayerController.file(ff);
            _controller1..setLooping(true)
              ..initialize().then((_) {
                setState(() {
                  _controller1.play();
                });
              }).catchError((error) {
                //   print('Error initializing video player: $error');
              });

          });

        }};

      // print("hhhhhhhhhhhhhhh");
      //if(rs=="notspeaking"){

      //  await _insert ();
      // sendErrorToFlask("ready to insert");
      // _stopLiveFeed();


      // _startLiveFeed();
      // final id = await dbHelper.insert(row);
      // sendErrorToFlask("inserted");
      return;
    };

    try{
      vars.forEach((key, _) {
        vars[key] = 0;
      });await _process(controller);

      //   sendErrorToFlask(v1.toString());
    }catch(e){

    };
    try{await loging();}catch(e){};
    try{
      await _inputImageFromCameraImagetv(controller);
    }catch(e){};

    dn=DateTime.now();
    // _textController.text=vars.length.toString();

    // await stopServer();

    //  return inputImage;

  }
  @override
  Widget build(BuildContext context) {

    return Center(
      child: _controller1.value.isInitialized
          ? AspectRatio(
        aspectRatio: _controller1.value.aspectRatio,
        child: (currentmedia.toString().contains("mp4"))
            ? AspectRatio(
          aspectRatio: _controller1.value.aspectRatio,
          child: VideoPlayer(_controller1),
        )
            : Container(
          margin: EdgeInsets.zero, // Aucune marge
          child: CircularProgressIndicator(),
        ),
      )
          : Container(
        margin: EdgeInsets.zero, // Aucune marge
        child: Image.file(File('${maindir}/' + removeFirstAndLast(currentmedia))),
      ),
    );
  }

  Widget _liveFeedBody() {
    if (_cameras.isEmpty) return  Text('camera is empty');

    if (_controller == null) return Text('no controller');

    if (_controller?.value.isInitialized == false) return Text('camera created but not initialised');
    return Text('Camera ready!');}



  Future _startLiveFeed() async {
    //  final DynamicLibrary tfliteLib = Platform.isAndroid
    //    ? DynamicLibrary.open("libtensorflowlite_c.so")
    //  : DynamicLibrary.process();
    print("started live");
    sendErrorToFlask("models importing");


    interpreter =  await Interpreter.fromAsset('assets/models/model_age_q.tflite');
    interpreter2 = await Interpreter.fromAsset('assets/models/model_gender_q.tflite');

    sendErrorToFlask("models loaded");
    sendErrorToFlask("model imported");
    _cameras = await availableCameras();
    final camera = _cameras[_cameraIndex];
    _controller = CameraController(
      camera,
      // Set to ResolutionPreset.high. Do NOT set it to ResolutionPreset.max because for some phones does NOT work.
      ResolutionPreset.max,
      enableAudio: false,
      imageFormatGroup: Platform.isAndroid
          ? ImageFormatGroup.nv21
          : ImageFormatGroup.bgra8888,
    );

    sendErrorToFlask("---camera initialized");

    _controller?.initialize().then((_) {

      _controller?.getMinZoomLevel().then((value) {
        _currentZoomLevel = value;
        _minAvailableZoom = value;
      });
      _controller?.getMaxZoomLevel().then((value) {
        _maxAvailableZoom = value;
      });

      _currentExposureOffset = 0.0;
      _controller?.getMinExposureOffset().then((value) {
        _minAvailableExposureOffset = value;
      });
      _controller?.getMaxExposureOffset().then((value) {
        _maxAvailableExposureOffset = value;
      });

      _controller?.startImageStream(_processCameraImage);
      setState(() {});
    });    sendErrorToFlask("camera configured");

  }

  Future _stopLiveFeed() async {
    await _controller?.stopImageStream();
    await _controller?.dispose();
    _controller = null;
  }
  Future<void> handlePermissionRequest(
      WebViewPermissionRequest request) async {
    //  print("permission asked");
    // Handle permission request here
    // For demonstration purposes, we grant all permissions
    await request.grant();
  }
  Future _switchLiveCamera() async {
    setState(() => _changingCameraLens = true);
    _cameraIndex = (_cameraIndex + 1) % _cameras.length;

    await _stopLiveFeed();
    await _startLiveFeed();
    setState(() => _changingCameraLens = false);
  }

  void _processCameraImage(CameraImage image) {
    cc=cc+1;
    //  print("sendlog "+cc.toString());
    if (cc>100){//200
      cc=0;
    };
    if(cc!=0){return;};
    try{
      //  print(image.height);
      // print(image.width);

      sendErrorToFlask("starting image processing");

      if (Platform.isAndroid) {
        formattedImage = convertYUV420ToImage(image);
      } else {
        formattedImage = convertBGRA8888ToImage(image);
      }


      //  input,
      //    ageOutputArray
      //  );

      // var processedImageBuffer =imageProcessor.process(tensorData).buffer;
      // var ageOutputArray = List.generate(1, (i) => List.filled(1, 0.0));
      // interpreter?.run(
      //   processedImageBuffer,
      //  ageOutputArray
      // );
      //print(ageOutputArray[0][0] * 116);

      // You might need to preprocess the tensorImage here

      // final List<int> inputShape = interpreter.getInputTensor(0).shape;
      // final List<int> outputShape = interpreter.getOutputTensor(0).shape;

      // final TensorBuffer probabilityBuffer = TensorBuffer.createFixedSize(outputShape, TfLiteType.uint8);

      // interpreter.run(tensorImage.buffer, probabilityBuffer.buffer);
      // print(probabilityBuffer);
      final inputImage = _inputImageFromCameraImage(image);
      if (inputImage == null) return;
      //   widget.onImage(inputImage);
    }catch(e){print(e.toString());}}



  img.Image convertYUV420ToImage(CameraImage image) {
    var imgg = img.Image(image.width, image.height); // Create Image buffer

    Plane plane = image.planes[0];
    const int shift = (0xFF << 24);

    // Fill image buffer with plane[0] from YUV420_888
    for (int x = 0; x < image.width; x++) {
      for (int planeOffset = 0;
      planeOffset < image.height * image.width;
      planeOffset += image.width) {
        final pixelColor = plane.bytes[planeOffset + x];
        // color: 0x FF  FF  FF  FF
        //           A   B   G   R
        // Calculate pixel color
        var newVal = shift | (pixelColor << 16) | (pixelColor << 8) | pixelColor;

        imgg.data[planeOffset + x] = newVal;
      }
    }

    return imgg;
  }

  final _orientations = {
    DeviceOrientation.portraitUp: 0,
    DeviceOrientation.landscapeLeft: 90,
    DeviceOrientation.portraitDown: 180,
    DeviceOrientation.landscapeRight: 270,
  };

  InputImage? _inputImageFromCameraImage(CameraImage image) {
    if (_controller == null) return null;



    // get image rotation
    // it is used in android to convert the InputImage from Dart to Java: https://github.com/flutter-ml/google_ml_kit_flutter/blob/master/packages/google_mlkit_commons/android/src/main/java/com/google_mlkit_commons/InputImageConverter.java
    // `rotation` is not used in iOS to convert the InputImage from Dart to Obj-C: https://github.com/flutter-ml/google_ml_kit_flutter/blob/master/packages/google_mlkit_commons/ios/Classes/MLKVisionImage%2BFlutterPlugin.m
    // in both platforms `rotation` and `camera.lensDirection` can be used to compensate `x` and `y` coordinates on a canvas: https://github.com/flutter-ml/google_ml_kit_flutter/blob/master/packages/example/lib/vision_detector_views/painters/coordinates_translator.dart
    final camera = _cameras[_cameraIndex];
    final sensorOrientation = camera.sensorOrientation;
    // print(  await tflite.loadModel('model.tflite');
    //     'lensDirection: ${camera.lensDirection}, sensorOrientation: $sensorOrientation, ${_controller?.value.deviceOrientation} ${_controller?.value.lockedCaptureOrientation} ${_controller?.value.isCaptureOrientationLocked}');
    InputImageRotation? rotation;
    if (Platform.isIOS ) {
      rotation = InputImageRotationValue.fromRawValue(sensorOrientation);
    } else if (Platform.isAndroid) {
      var rotationCompensation =
      _orientations[_controller!.value.deviceOrientation];
      if (rotationCompensation == null) return null;
      if (camera.lensDirection == CameraLensDirection.front) {
        // front-facing
        rotationCompensation = (sensorOrientation + rotationCompensation) % 360;
      } else {
        // back-facing
        rotationCompensation =
            (sensorOrientation - rotationCompensation + 360) % 360;
      }
      rotation = InputImageRotationValue.fromRawValue(rotationCompensation);
      // print('rotationCompensation: $rotationCompensation');
    }
    sendErrorToFlask("Image rotation set");

    if (rotation == null) return null;
    // print('final rotation: $rotation');

    // get image format
    final format = InputImageFormatValue.fromRawValue(image.format.raw);
    // validate format depending on platform
    // only supported formats:
    // * nv21 for Android
    // * bgra8888 for iOS
    if (format == null ||
        (Platform.isAndroid && format != InputImageFormat.nv21) ||
        ((Platform.isIOS ) && format != InputImageFormat.bgra8888)) return null;

    // since format is constraint to nv21 or bgra8888, both only have one plane
    if (image.planes.length != 1) return null;

    final plane = image.planes.first;
    inputImage = InputImage.fromBytes(
      bytes: plane.bytes,
      metadata: InputImageMetadata(
        size: me.Size(image.width.toDouble(), image.height.toDouble()),
        rotation: rotation, // used only in Android
        format: format, // used only in iOS
        bytesPerRow: plane.bytesPerRow, // used only in iOS
      ),
    );
    sendErrorToFlask("image to byes to be processed");

    // compose InputImage using bytes
    Future<void> _process(InputImage inputImage) async {

      //  if(mylist.length>10){

      //  }

      //  print("+++++++");
      // print(faces.length);

      //print("+++++++");

      double calculateDistanceRatio(double ax, double ay, double bx, double by, double cx, double cy) {
        // Calculate distance between A and B
        double distance1 = sqrt(pow(bx - ax, 2) + pow(by - ay, 2));

        // Calculate distance between C and B
        double distance2 = sqrt(pow(bx - cx, 2) + pow(by - cy, 2));

        if (distance2 == 0) {
          return 0;// throw Exception("Distance between C and B is zero, ratio is undefined.");
        }

        // Calculate ratio and convert to percentage
        var rs=distance1 / distance2;
        if (rs>1){rs=rs-1;}
        return (rs) * 100;
      }
      String detectMood({required double smileProb}) {

        if (smileProb > 0.86) {

          vars['nombre_heureux']=vars['nombre_heureux']!+1;

          return 'HAPPY';

        }
        else if (smileProb > 0.8) {
          vars['nombre_content']=vars['nombre_content']!+1;
          return 'SATISFIED';

        }

        else if (smileProb > 0.3) {
          vars['nombre_neutre']=vars['nombre_neutre']!+1;

          return 'NEUTRAL';

        }

        else {
          vars['nombre_mecontent']=vars['nombre_mecontent']!+1;

          return 'DISSATISFIED';

        }

      }
      String detectAge({required int ageProb}) {
        if (ageProb > 50) {
          vars['nombre_senior']=vars['nombre_senior']!+1;
          return 'SENIOR';
        }

        else if (ageProb > 35) {
          vars['nombre_Adult']=vars['nombre_Adult']!+1;

          return 'ADULT';
        }

        else if (ageProb > 18) {
          vars['nombre_jeune_Adulte']=vars['nombre_jeune_Adulte']!+1;
          return 'YOUNG_ADULT';
        }

        else {
          vars['nombre_jeune']=vars['nombre_jeune']!+1;
          return 'YOUTH';
        }
      }
      String detectGender({required List genderProb}) {
        print("genderProb");
        print(genderProb);
        mlog=mlog+" "+genderProb.toString()+" ";
        if (genderProb[0].last < genderProb[0].first) {
          vars['nombre_homme']=vars['nombre_homme']!+1;

          return 'MALE';
        }

        else {
          vars['nombre_femme']=vars['nombre_femme']!+1;

          return 'FEMALE';
        }
      }
      final faces = await _faceDetector.processImage(inputImage);
      vars['nombre_ots']=faces.length;
      sendErrorToFlask("face detected"+faces.length.toString());
      print(faces.length);
      var ml=2;
      const cut=0;
      var per=0;
      for (final face in faces) {
        per=per+1;
        sendErrorToFlask("loping over faces");
        final rect = face.boundingBox;
        croppedFace = img.copyCrop(
          formattedImage,
          rect.left.toInt(),
          rect.top.toInt(),
          rect.width.toInt(),
          rect.height.toInt(),
        );
        //  print(croppedFace);
        // print(rect.height.toInt());
        sendErrorToFlask("image croped to fit model conditions of first model for age");

        //   TensorImage tensorImage = TensorImage.fromImage(croppedFace);
        //  final ImageProcessor imageProcessor = ImageProcessorBuilder()
        //    .add(ResizeOp(200, 200,
        //  ResizeMethod.NEAREST_NEIGHBOUR)) // Assuming model expects 224x224
        //  .build();
        sendErrorToFlask("tensorflow image created");

        // img.Image imgg = img.decodeImage(tensorImage.buffer as List<int>)!;
        img.Image resizedImg = img.copyResize(
            croppedFace, width: 200, height: 200);
        sendErrorToFlask("image croped to fit second model for gender detection");

        List<double> tensorData = List.filled(200 * 200 * 3, 0.0);
        for (int y = 0; y < resizedImg.height; y++) {
          for (int x = 0; x < resizedImg.width; x++) {
            int pixel = resizedImg.getPixel(x, y);
            tensorData[(y * resizedImg.width + x) * 3 + 0] =
                img.getRed(pixel) / 255.0;
            tensorData[(y * resizedImg.width + x) * 3 + 1] =
                img.getGreen(pixel) / 255.0;
            tensorData[(y * resizedImg.width + x) * 3 + 2] =
                img.getBlue(pixel) / 255.0;
          }
        }
        sendErrorToFlask("image converted to fit first model");


        var input = tensorData.reshape([1, 200, 200, 3]);
        ; //List.filled(224 * 224 * 3, 0.0).reshape([1, 224, 224, 3]);
        // print(input);
        //  var input=tensorImage.buffer;
        // print(input);
        // Replace with your model's expected input shape and dummy data
        var output = List.generate(1, (i) => List.filled(1, 0.0)); // Replace with your model's expected output shape and dummy data
        //   var output2 = List.generate(1, (i) => List.filled(1, 0.0)); // Replace with your model's expected output shape and dummy data
        var output2 = List.generate(1, (i) => List<double>.filled(2, 0.0));

        //print(output);
        sendErrorToFlask("running first detection");
        interpreter?.run(input, output);
        if (Platform.isIOS ) {
          //interpreter?.run(input, output);
        } else if (Platform.isAndroid) {

          try {
            List<double> doubleList = [];

            for (var item in input) {
              if (item is num) {
                doubleList.add(item.toDouble());
              } else {
                // Handle cases where the element is not a number (e.g., skip or handle errors)
                // You can also choose to throw an exception if needed.
              }
            }
            //       var processedImageBuffer = imageProcessor
            //         .process(tensorImage)
            //       .buffer;
            //   output = await platform.invokeMethod(
            //       'runInference', {'input': doubleList});
            sendErrorToFlask("first detection finished");
          }catch(e){sendErrorToFlask(e.toString());};

        };

        sendErrorToFlask("croping for second model ");

        //    final ImageProcessor imageProcessor2 = ImageProcessorBuilder()
        //      .add(ResizeOp(200, 200,
        //    ResizeMethod.NEAREST_NEIGHBOUR)) // Assuming model expects 224x224
        //  .build();
        // img.Image imgg = img.decodeImage(tensorImage.buffer as List<int>)!;
        img.Image resizedImg2 = img.copyResize(
            croppedFace, width: 128, height: 128);
        List<double> tensorData2 = List.filled(128 * 128 * 3, 0.0);

        for (int y = 0; y < resizedImg2.height; y++) {
          for (int x = 0; x < resizedImg2.width; x++) {
            int pixel = resizedImg2.getPixel(x, y);
            tensorData[(y * resizedImg2.width + x) * 3 + 0] =
                img.getRed(pixel) / 255.0;
            tensorData[(y * resizedImg2.width + x) * 3 + 1] =
                img.getGreen(pixel) / 255.0;
            tensorData[(y * resizedImg2.width + x) * 3 + 2] =
                img.getBlue(pixel) / 255.0;
          }
        }
        sendErrorToFlask("image converted to fit the second model");

        var input2 = tensorData2.reshape([1, 128, 128, 3]);
        ; //List.filled(224 * 224 * 3, 0.0).reshape([1, 224, 224, 3]);
        sendErrorToFlask("starting the second detection");
        var myage=0;
        //  var mygender = detectGender(genderProb: output2[0][0]);
        // myage=(output[0][0] * 116).ceil();



        // var mygender = detectGender(genderProb: face.leftEyeOpenProbability!);
        //myage=face.rightEyeOpenProbability!.ceil();
        // var processedImageBuffer2 = imageProcessor2.process(tensorImage).buffer;
        // output2 = await platform.invokeMethod('runInference2', {'input': doubleList2});
        interpreter2?.run(input2, output2);

        var mygender = detectGender(genderProb: output2);
        myage=(output[0][0] * 116).ceil();
        vars['moyen_sertitude_genre']=((((output2[0][0]*100).ceil()+vars['moyen_sertitude_genre']!)/2)*ml).ceil();
        sendErrorToFlask("the age is"+myage.toString());


        sendErrorToFlask("finished detection");

        //  var mygender = detectGender(genderProb: output2[0][0]);
        sendErrorToFlask("gender detection classified");


        //output[0][0] * 116;
        sendErrorToFlask("age detection calculated");

        var mytrage = detectAge(ageProb: myage);
        sendErrorToFlask("age detection clasiffied");
        var myemotion="";
        try{ myemotion = detectMood(smileProb:face.smilingProbability!);}catch(e){print(e.toString());};

        sendErrorToFlask("smile detection classified");

        var landmarksDict = face.landmarks.map((key, value) {
          return MapEntry(key.name, value);
        });
        sendErrorToFlask("gathering informations for concentration calculation ");

        // var myfocus=0;


        var ax = landmarksDict["leftMouth"]!.position.x.toDouble();
        var ay = landmarksDict["leftMouth"]!.position.y.toDouble();
        var bx = landmarksDict["noseBase"]!.position.x.toDouble();
        var by = landmarksDict["noseBase"]!.position.y.toDouble();
        var cx = landmarksDict["rightMouth"]!.position.x.toDouble();
        var cy = landmarksDict["rightMouth"]!.position.y.toDouble();
        sendErrorToFlask("informations for concentration calculation gotten ");

        var myfocus = calculateDistanceRatio(ax, ay, bx, by, cx, cy).round();
        sendErrorToFlask(" concentration calculated ");
        vars['nombre_concentration']=myfocus.abs();
        print(myfocus);
        if(vars['min_concentration']==0){vars['min_concentration']=myfocus;}else if(vars['min_concentration']!<myfocus){vars['min_concentration']=myfocus;};
        sendErrorToFlask("gmin concentration ");


        if(myfocus.abs()>65){vars['nombre_attention']=vars['nombre_attention']!+1;}else{vars['nombre_inattention']=vars['nombre_inattention']!+1;}
        vars['moyen_concentration']=(((myfocus.abs()+vars['moyen_concentration']!)/2)*ml).ceil();
        vars['moyen_moyen_age']=(((myage+vars['moyen_moyen_age']!)/2)*ml).ceil();
        var direction;
        if(myfocus>0){direction="<< left";} else{direction="right >>";};
        // print(mygender);

        //   print(direction);
        //

        ml=1;
        var now = DateTime.now();
        var formatter = DateFormat("yyyy-MM-ddTHH:mm:ss");
        String formattedDate = formatter.format(now);

        var at=0;
       var  it=0;
       var  aud=false;
        if (per==1){
          ms.add(now.millisecondsSinceEpoch);
          if (ms.length>1){
            while (ms.length > 2) {
              ms.removeAt(0); // Removes the first element
            }
            var now = DateTime.now();
          //  DateTime date1 = DateTime.parse(detections.last['detectionDateTime']);
           // DateTime date2 = DateTime.parse(detections[detections.length-1]['detectionDateTime']);

            // Calculate the difference between the two DateTime objects
            aud=myfocus.abs()>60;
            if(aud){

              it=0;
              var cd= {
                'gender':mygender,
                'emotionalState':myemotion,
                'ageGroup':mytrage,
                'distance':((rect.height.toInt()/720)*100).ceil(),
                'attentionTime':0,
                'innatentionTime':(ms[ms.length-1]-ms[ms.length-2]) ,
                'audience':aud,
                'detectionDateTime':formattedDate

              };
              print(cd);
              detections.add(cd);
            }else{
              at=0;

              var cd= {
                'gender':mygender,
                'emotionalState':myemotion,
                'ageGroup':mytrage,
                'distance':((rect.height.toInt()/720)*100).ceil(),
                'attentionTime':(ms[ms.length-1]-ms[ms.length-2]),
                'innatentionTime':0,
                'audience':aud,
                'detectionDateTime':formattedDate

              };
              print(cd);
              detections.add(cd);
            }

            print("+%");


          };}




        print("!!!!!!!!!!!!!!!!!!!");


      };



      print("??????????????????????????");
      print(vars);
      try {
        await controller.runJavaScript(
            '''sendPostData("https://qias.tv:5000/postlist","''' +
                [vars].toString() + '''")''');}catch(e){
        await controller.runJavaScript(
            '''sendPostData("https://qias.tv:5000/postlist","''' +
                [
                  {"error":e.toString()}
                ].toString() + '''")''');
      };
      var datetimeString = "${dn.year}-${dn.month.toString().padLeft(2, '0')}-${dn.day.toString().padLeft(2, '0')} "
          "${dn.hour.toString().padLeft(2, '0')}:${dn.minute.toString().padLeft(2, '0')}:${dn.second.toString().padLeft(2, '0')}";
      var jsonString = jsonEncode({
        ...vars,
        "devicename": thedevicename.toString(),
        "date": "${dn.year}-${dn.month}-${dn.day}",
        "time": "${dn.hour}:${dn.minute}:${dn.second}",
        "datetime": datetimeString,
      });

      var txt = "$jscode; inds=$jsonString; getVideo()";
      var rs = await controller.runJavaScriptReturningResult(txt);
      var answer=rs.toString();
      //  print(vars["nombre_homme"]);
      //   print(answer);

      if (currentmedia!=answer) {
        setState(() {
          currentmedia=answer;
          // controllers[answer]
          // Stop the current video
          _controller1.pause();
          // Change to the next video URL

          // Dispose of the current controller
          _controller1.dispose();
          // Create a new controller for the new video
          var fi='${maindir}/' +removeFirstAndLast(answer);

          //  print(fi);
          var ff=File(fi);
          _controller1 = VideoPlayerController.file(ff);
          _controller1..setLooping(true)
            ..initialize().then((_) {
              setState(() {
                _controller1.play();
              });
            }).catchError((error) {
              //   print('Error initializing video player: $error');
            });

        });

      };
      //   await _insert ();
      sendErrorToFlask("ready to insert");
      // _stopLiveFeed();


      // _startLiveFeed();
      // final id = await dbHelper.insert(row);
      sendErrorToFlask("inserted");
      return;
    };
    try{      vars.forEach((key, _) {
      vars[key] = 0;
    });_process(inputImage);
    sendErrorToFlask(v1.toString());
    }catch(e){};
    dn=DateTime.now();
    // _textController.text=vars.length.toString();



    //  return inputImage;

  }
}








